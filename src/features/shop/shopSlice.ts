import { BasketProduct } from "./../../API/BasketAPI";
import { RootState } from "./../../redux/store";
import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Product } from "../../types/Product";
import { fetchProductsParams, ProductsAPI } from "../../API/ProductAPI";
import { CategoriesAPI, ICategory } from "../../API/CategoriesAPI";
import { FavoriteAPI } from "../../API/FavoriteAPI";
import { BasketAPI } from "../../API/BasketAPI";

export const fetchProducts = createAsyncThunk(
  "product/fetchProducts",
  async (params: fetchProductsParams, thunkAPI) => {
    return ProductsAPI.get(params);
  }
);

export const fetchFilterParams = createAsyncThunk(
  "product/fetchFilterParams",
  async (param: string, thunkAPI) => {
    return ProductsAPI.getFilter(param);
  }
);

export const fetchCategories = createAsyncThunk(
  "product/categories",
  async (params, thunkAPI) => {
    const cats = await CategoriesAPI.getAll();
    thunkAPI.dispatch(setCategoriesAC(cats));
    return cats;
  }
);

export const fetchProduct = createAsyncThunk(
  "product/product",
  async (id: string, thunkAPI) => {
    return ProductsAPI.getOne(id);
  }
);

export const fetchFavorities = createAsyncThunk(
  "product/favorities",
  async (params, thunkAPI) => {
    const favs = await FavoriteAPI.getAll();
    thunkAPI.dispatch(setFavoriteAC(favs));
    return favs;
  }
);

export const fetchBasket = createAsyncThunk(
  "product/favorities",
  async (params, thunkAPI) => {
    const basket = await BasketAPI.getAll();
    thunkAPI.dispatch(setBasketAC(basket));
    return basket;
  }
);

interface ShopState {
  products: Array<Product>;
  categories: Array<ICategory> | null;
  currentCategory: string | null;
  favorites: Array<Product> | null;
  basket: Array<BasketProduct> | null;
}

const initialState: ShopState = {
  products: [],
  categories: null,
  currentCategory: null,
  favorites: null,
  basket: null,
};

export const shopSlice = createSlice({
  name: "shop",
  initialState,
  reducers: {
    setProductsAC: (state, action: PayloadAction<Array<Product>>) => {
      state.products = action.payload;
    },
    setCategoriesAC: (state, action: PayloadAction<Array<ICategory>>) => {
      state.categories = action.payload;
    },
    setCurrentCategory: (state, action: PayloadAction<string>) => {
      state.currentCategory = action.payload;
    },
    setFavoriteAC: (state, action: PayloadAction<Array<Product>>) => {
      state.favorites = action.payload;
    },
    addFavoriteAC: (state, action: PayloadAction<Product>) => {
      if (state.favorites === null) state.favorites = [];
      state.favorites.push(action.payload);
    },
    delFavoriteAC: (state, action: PayloadAction<Product>) => {
      if (state.favorites === null) state.favorites = [];
      state.favorites = state.favorites.filter(
        (prod) => prod.id !== action.payload.id
      );
    },
    setBasketAC: (state, action: PayloadAction<Array<BasketProduct>>) => {
      state.basket = action.payload;
    },
    addBasketAC: (state, action: PayloadAction<BasketProduct>) => {
      if (state.basket === null) state.basket = [];
      state.basket.push(action.payload);
    },
    delBasketAC: (state, action: PayloadAction<BasketProduct>) => {
      if (state.basket === null) state.basket = [];
      state.basket = state.basket.filter(
        (bprod) => bprod.product.id !== action.payload.product.id
      );
    },
    updateBasketAC: (state, action: PayloadAction<BasketProduct>) => {
      if (state.basket === null) {
        state.basket = [];
      } else {
        state.basket = state.basket.map((bprod) => {
          if (bprod.product.id === action.payload.product.id) {
            return action.payload;
          }
          return bprod;
        });
      }
    },
  },
  // extraReducers(builder) {
  //   builder.addCase(updateBasketAC, (state, action) => {
  //     console.log(action);
  //   });
  // },
});

export const {
  setProductsAC,
  setCategoriesAC,
  setCurrentCategory,
  setFavoriteAC,
  addFavoriteAC,
  delFavoriteAC,
  setBasketAC,
  addBasketAC,
  delBasketAC,
  updateBasketAC,
} = shopSlice.actions;

export const selectProducts = (state: RootState) => state.shop.products;
export const selectCategories = (state: RootState) => state.shop.categories;
export const selectCurrentCategory = (state: RootState) =>
  state.shop.currentCategory;
export const selectFavorites = (state: RootState) => state.shop.favorites;
export const selectBasket = (state: RootState) => state.shop.basket;

export default shopSlice.reducer;
