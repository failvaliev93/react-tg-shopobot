import { createTheme } from "@mui/material";

declare module "@mui/material/styles" {
  interface Theme {
    status: {
      danger: string;
    };
  }
  // allow configuration using `createTheme`
  interface ThemeOptions {
    status?: {
      danger?: string;
    };
  }
}

const theme = createTheme({
  // status: {
  //   danger: orange[500],
  // },
  palette: {
    primary: {
      main: "#000",
      dark: "#000",
    },
    success: {
      main: "#31B545",
      contrastText: "#fff",
    },
  },
  typography: {
    fontFamily: `"Montserrat", -apple-system, BlinkMacSystemFont, "Segoe UI",
    "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans",
    "Helvetica Neue", sans-serif`,
    h4: {
      fontSize: "1.9rem",
      fontWeight: 500,
    },
    subtitle2: {
      fontSize: "13px",
      fontWeight: 400,
    },
  },
  shape: {
    borderRadius: 10,
  },
});

theme.components = {
  MuiSlider: {
    defaultProps: {
      color: "primary",
    },
    styleOverrides: {
      valueLabel: {
        display: "none",
      },
      active: {
        boxShadow: "none",
      },
      thumb: {
        boxShadow: "none !important",
      },
      focusVisible: {
        boxShadow: "none",
      },
    },
  },
  MuiButtonBase: {
    defaultProps: {
      disableRipple: true,
    },
    styleOverrides: {},
  },
  MuiIconButton: {
    defaultProps: {
      disableRipple: true,
    },
  },
  MuiRadio: {
    defaultProps: {
      disableRipple: true,
    },
    styleOverrides: {
      root: {
        ":hover": {
          backgroundColor: "initial",
        },
      },
    },
  },
};

export default theme;
