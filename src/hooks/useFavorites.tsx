import React from "react";
import { useSelector } from "react-redux";
import { fetchFavorities, selectFavorites } from "../features/shop/shopSlice";
import { useAppDispatch } from "../redux/hooks";
import { Product } from "../types/Product";

export function useFavorites(): Array<Product> | null {
  const dispatch = useAppDispatch();
  const allFavorites = useSelector(selectFavorites);

  React.useEffect(() => {
    if (allFavorites === null) {
      dispatch(fetchFavorities());
    }
  }, [allFavorites]);

  return allFavorites;
}
