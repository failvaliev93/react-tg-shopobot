import { useState } from "react";

export function useToggle(initValue: boolean): [boolean, () => void] {
  const [value, setValue] = useState<boolean>(initValue);
  const toggle = () => setValue(!value);

  return [value, toggle];
}
