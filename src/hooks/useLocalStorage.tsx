import { useState, useEffect } from "react";

export function useLocalStorage<T>(
  initialValue: T,
  key: string
): [T, (value: T) => void] {
  const getValue = (): T => {
    const storage: string | null = localStorage.getItem(key);

    if (storage) {
      return JSON.parse(storage) as T;
    }

    return initialValue;
  };

  const [value, setValue] = useState<T>(getValue);

  useEffect(() => {
    localStorage.setItem(key, JSON.stringify(value));
  }, [value]);

  return [value, setValue];
}
