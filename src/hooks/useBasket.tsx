import React from "react";
import { useSelector } from "react-redux";
import { BasketProduct } from "../API/BasketAPI";
import { fetchBasket, selectBasket } from "../features/shop/shopSlice";
import { useAppDispatch } from "../redux/hooks";

export function useBasket(): Array<BasketProduct> | null {
  const dispatch = useAppDispatch();
  const basket = useSelector(selectBasket);

  React.useEffect(() => {
    if (basket === null) {
      dispatch(fetchBasket());
    }
  }, [basket]);

  return basket;
}
