import React from "react";
import { useSelector } from "react-redux";
import { ICategory } from "../API/CategoriesAPI";
import { fetchCategories, selectCategories } from "../features/shop/shopSlice";
import { useAppDispatch } from "../redux/hooks";

export function useCategories(): Array<ICategory> | null {
  const dispatch = useAppDispatch();
  const categories = useSelector(selectCategories);

  React.useEffect(() => {
    if (categories === null) {
      dispatch(fetchCategories());
    }
  }, [categories]);

  return categories;
}
