import React from "react";
import { Mobile } from "./layouts/Mobile";

function App() {
  return (
    <div>
      <Mobile />
    </div>
  );
}

export default App;
