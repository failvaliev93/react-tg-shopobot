import React from "react";
import { FavoriteListWithHandler } from "../../components/shop/content/favorite/favoriteListWithHandler/FavoriteListWithHandler";
import { Page } from "../../components/shop/page/Page";

export const FavoritePage = () => {
  return (
    <Page pageName="Избранное">
      <FavoriteListWithHandler />
    </Page>
  );
};
