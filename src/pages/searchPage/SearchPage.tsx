import React from "react";
import { ICategory } from "../../API/CategoriesAPI";
import { Categories } from "../../components/shop/content/categories/Categories";
import { ProductSearch } from "../../components/UI/inputs/search/productSearch/ProductSearch";
import { ProductFeed } from "../../components/shop/content/products/ProductFeed";
import { useCategories } from "../../hooks/useCategories";
import { Page } from "../../components/shop/page/Page";

export const SearchPage = () => {
  const categories = useCategories();
  const [currentCategory, setCurrentCategory] =
    React.useState<ICategory | null>(null);
  const [searchValue, setSearchValue] = React.useState<string>("");

  const categorySelectHandler = (category: ICategory) => {
    setCurrentCategory(category);
  };

  const searchHandler = (searchValue: string) => {
    if (searchValue) {
      setSearchValue(searchValue);
    } else {
      setSearchValue("");
    }
    setCurrentCategory(null);
  };

  return (
    <Page>
      {!currentCategory && (
        <ProductSearch
          onSearch={searchHandler}
          sx={{
            marginTop: "20px",
          }}
        />
      )}
      <div>
        {categories !== null && !currentCategory && !searchValue && (
          <Categories items={categories} onSelect={categorySelectHandler} />
        )}
        {(currentCategory || searchValue) && (
          <>
            <ProductFeed
              queryParams={{
                category: currentCategory?.name || "",
                search: {
                  word: searchValue,
                },
              }}
            />
          </>
        )}
      </div>
    </Page>
  );
};
