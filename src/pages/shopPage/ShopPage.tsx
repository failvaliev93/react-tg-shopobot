import React from "react";
import { Banner } from "../../components/shop/content/banner/Banner";
import { ProductFeed } from "../../components/shop/content/products/ProductFeed";
import { Page } from "../../components/shop/page/Page";

import s from "./ShopPage.module.css";

const ShopPage = () => {
  const title = <div className={s.pageTitle}>ShopoBot</div>;

  return (
    <Page>
      {title}
      <Banner />
      <ProductFeed />
    </Page>
  );
};

export default ShopPage;
