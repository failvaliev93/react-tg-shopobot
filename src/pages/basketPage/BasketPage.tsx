import React from "react";
import { BasketWithHandler } from "../../components/shop/content/basket/basketWithHandler/BasketWithHandler";
import { Page } from "../../components/shop/page/Page";

export const BasketPage = () => {
  return (
    <Page pageName="Корзина">
      <BasketWithHandler />
    </Page>
  );
};
