import React, { FC } from "react";
import { useParams } from "react-router-dom";
import { ProductFeed } from "../../components/shop/content/products/ProductFeed";
import { Page } from "../../components/shop/page/Page";
import { useCategories } from "../../hooks/useCategories";

export const CategoryPage: FC = () => {
  const { category } = useParams();

  const categories = useCategories();
  const [currentCategoryName, setCurrentCategoryName] =
    React.useState<string>("");

  React.useEffect(() => {
    if (categories !== null) {
      categories.forEach((cat) => {
        if (cat.adres === category) {
          setCurrentCategoryName(cat.name);
        }
      });
    }
  }, [categories]);

  return (
    <div>
      <Page pageName={currentCategoryName} backButton>
        {currentCategoryName && (
          <ProductFeed
            queryParams={{
              category: currentCategoryName,
            }}
          />
        )}
      </Page>
    </div>
  );
};
