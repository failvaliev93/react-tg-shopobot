import React from "react";
import { useParams } from "react-router-dom";
import { Typography } from "@mui/material";
import { fetchProduct } from "../../features/shop/shopSlice";
import { useAppDispatch } from "../../redux/hooks";
import { Product } from "../../types/Product";
import { ProductCard } from "../../components/shop/content/productCard/ProductCard";
import s from "./ProductPage.module.css";
import { BackButton } from "../../components/UI/buttons/backButton/BackButton";

export const ProductPage = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams();
  const [product, setProduct] = React.useState<Product | null>(null);
  const [error, setError] = React.useState<string | null>(null);

  React.useEffect(() => {
    if (id) {
      dispatch(fetchProduct(id))
        .then((res) => {
          // console.log(res);
          if (res.meta.requestStatus !== "rejected" && res.payload) {
            setProduct(res.payload as Product);
          } else {
            setError("Не удалось загрузить товар.");
          }
        })
        .catch(() => {
          setError("Не удалось загрузить товар.");
        });
    } else {
      setError("Не удалось загрузить товар.");
    }
  }, []);

  return (
    <div className={s.productPage}>
      <div className={s.backWrap}>
        <BackButton />
      </div>
      {product && !error ? <ProductCard product={product} /> : ""}
      {error && (
        <Typography
          sx={{
            paddingTop: "50px",
          }}
        >
          {error}
        </Typography>
      )}
    </div>
  );
};
