import { Product } from "../types/Product";
import { BaseAPI } from "./BaseAPI";

export interface BasketProduct {
  count: number;
  product: Product;
}

export class BasketAPI {
  static async getAll(): Promise<Array<BasketProduct>> {
    const { data } = await BaseAPI.get("/basket");
    return data;
  }
}
