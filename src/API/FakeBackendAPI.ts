import {
  commonProperties,
  ICommonProperties,
  IShoeProperties,
  shoeProperties,
} from "./mock/properties";
import { categoriesURL } from "./mock/categories";
import { mockProducts } from "./mock/mockProducts";
import { Product } from "../types/Product";
import { fetchProductsParams } from "./ProductAPI";
import { favorites } from "./mock/favorites";
import { basket } from "./mock/basket";

export class FakeBackendAPI {
  static async response(
    method: "GET" | "POST" | "PUT" | "DELETE",
    url: string,
    queryParams?: Record<string, any>,
    data?: Record<string, any> | FormData,
    extraHeaders?: Record<string, any>
  ): Promise<Response> {
    return new Promise((resolve, reject) => {
      switch (url) {
        case "/categories":
          resolve(
            this.createResponce(200, {
              data: CategoryFAPI.get(),
            })
          );
          break;
        case "/products":
          resolve(
            this.createResponce(200, {
              data: ProductsFAPI.get(queryParams as fetchProductsParams),
            })
          );
          break;
        case "/product":
          const { id } = queryParams as { id: string };
          resolve(
            this.createResponce(200, {
              data: ProductsFAPI.getOne(id),
            })
          );
          break;
        case "/product/filter":
          const { category } = queryParams as { category: string };
          resolve(
            this.createResponce(200, {
              data: ProductsFAPI.getFilter(category),
            })
          );
          break;
        case "/favorites":
          resolve(
            this.createResponce(200, {
              data: FavoriteFAPI.getAll(),
            })
          );
          break;
        case "/basket":
          resolve(
            this.createResponce(200, {
              data: BasketFAPI.getAll(),
            })
          );
          break;
        default:
          resolve(this.createResponce(404, undefined, 0));
      }
    });
  }

  private static createResponce(
    status: number,
    data?: any,
    statusOk: 0 | 1 = 1
  ) {
    data.statusOk = statusOk;

    const blob = new Blob([JSON.stringify(data, null, 2)]);

    const r = new Response(blob, {
      status,
    });

    return r;
  }
}

class CategoryFAPI {
  static get() {
    return categoriesURL;
  }
}

interface IProductProperties extends ICommonProperties, IShoeProperties {}

class ProductProperties {
  static get(category: string): IProductProperties {
    switch (category) {
      case "Кроссовки":
        return { ...commonProperties, ...shoeProperties };
      default:
        return { ...commonProperties } as IProductProperties;
    }
  }
}

class ProductsFAPI {
  static get(params: fetchProductsParams) {
    let fProd = ProductsFAPI.getAll();

    const props = ProductProperties.get(params.category || "");
    const colorsAll: Array<string> = props?.colors || [];
    const sizesAll: Array<string> = props?.sizes || [];

    if (params.category && params?.category !== "all") {
      fProd = fProd.filter(
        (prod: Product) => prod.type.name === params.category
      );
    }

    if (params.search?.word) {
      const searchWord = params.search.word.toLowerCase();
      fProd = fProd.filter(
        (prod) =>
          prod.brand.toLowerCase().indexOf(searchWord) > -1 ||
          prod.description.toLowerCase().indexOf(searchWord) > -1 ||
          prod.text.toLowerCase().indexOf(searchWord) > -1
      );
    }

    const priceLimitMin =
      fProd.length > 0 ? Math.min(...fProd.map((p) => p.price)) : 0;
    const priceLimitMax =
      fProd.length > 0 ? Math.max(...fProd.map((p) => p.price)) : 0;

    if (params.properties?.colors?.length) {
      fProd = fProd.filter(
        (prod) =>
          prod.colors &&
          prod.colors.some((c) => params.properties?.colors?.includes(c))
      );
    }

    if (params.properties?.sizes?.length) {
      fProd = fProd.filter(
        (prod) =>
          prod.sizes &&
          prod.sizes.some((s) => params.properties?.sizes?.includes(s))
      );
    }

    if (params.properties?.discount) {
      if (params.properties.discount !== "all") {
        fProd = fProd.filter((prod) => {
          if (params.properties?.discount === "withDiscount") {
            return prod.discount;
          } else if (params.properties?.discount === "withoutDiscount") {
            return !prod.discount;
          }
          return false;
        });
      }
    }

    if (params.properties?.gender) {
      if (params.properties.gender !== "all") {
        fProd = fProd.filter((prod) => {
          if (params.properties?.gender === "f") {
            return prod.gender !== "m";
          } else if (params.properties?.gender === "m") {
            return prod.gender !== "f";
          }
          return false;
        });
      }
    }

    if (params.properties?.price) {
      if (params.properties?.price?.min) {
        fProd = fProd.filter(
          (prod) => prod.price >= (params.properties?.price?.min || 0)
        );
      }
      if (params.properties?.price?.max) {
        fProd = fProd.filter(
          (prod) => prod.price <= (params.properties?.price?.max || 0)
        );
      }
    }

    const productsCount = fProd.length;

    if (params.properties?.sort) {
      if (params.properties.sort === "priceLow") {
        fProd = fProd.sort((a, b) => a.price - b.price);
      } else if (params.properties.sort === "priceHigh") {
        fProd = fProd.sort((a, b) => b.price - a.price);
      }
    }

    const limit = params.limit || 20;
    const page = fProd.length > 0 ? params.page || 1 : 0;
    const maxPages = Math.ceil(fProd.length / limit) || page;
    // if (params.page && params.page > 1 && maxPages > 1) {
    // fProd = fProd.slice(limit * page, limit).filter(Boolean);
    if (page) {
      fProd = fProd.splice(limit * page - limit, limit).filter(Boolean);
    }
    // }

    return {
      list: fProd,
      productsCount,
      maxPages,
      page,
      priceLimitMin,
      priceLimitMax,
      sizesAll,
      colorsAll,
    };
  }

  static getOne(id: string) {
    return ProductsFAPI.getAll().find((prod) => id === prod.id);
  }

  static getFilter(category: string) {
    let filter: fetchProductsParams = {
      category: category,
      properties: {
        colors: [],
        sizes: [],
        discount: "all",
        gender: "all",
        price: {
          min: 0,
          max: 1000000,
        },
        sort: "priceLow",
      },
    };

    return filter;
  }

  private static getAll() {
    return mockProducts;
  }
}

class FavoriteFAPI {
  static getAll() {
    return favorites;
  }
}

class BasketFAPI {
  static getAll() {
    return basket;
  }
}
