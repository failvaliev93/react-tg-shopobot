import { Product } from "../types/Product";
import { BaseAPI } from "./BaseAPI";

export class FavoriteAPI {
  static async getAll(): Promise<Array<Product>> {
    const { data } = await BaseAPI.get("/favorites");
    return data;
  }
}
