import { API_REFERER } from "../consts/setup";
import { FakeBackendAPI } from "./FakeBackendAPI";

export class APIError extends Error {
  constructor(public code: string, public message: string) {
    super();
  }
}

export class BaseAPI {
  private static API_REFERER = API_REFERER;
  private static API_PREFIX = "";

  static async get(
    url: string,
    queryParams?: Record<string, any>,
    extraHeaders?: Record<string, any>
  ): Promise<any> {
    return this.fetch("GET", url, queryParams, undefined, extraHeaders);
  }

  private static async fetch(
    method: "GET" | "POST" | "PUT" | "DELETE",
    url: string,
    queryParams?: Record<string, any>,
    data?: Record<string, any> | FormData,
    extraHeaders?: Record<string, any>
  ) {
    const response = await FakeBackendAPI.response(
      method,
      url,
      queryParams,
      data,
      extraHeaders
    );
    const responseJson = await response.json();

    if (response.status !== 200 && response.status !== 201) {
      throw new APIError(responseJson.code, responseJson.message);
    }
    return responseJson;
  }

  private static generateQueryParams(params: Record<string, any>) {
    let query = "";
    for (const key of Object.keys(params)) {
      const value = params[key];
      if (value !== -1 && value !== "null" && (value || value === 0)) {
        query += query ? `&${key}=${params[key]}` : `?${key}=${params[key]}`;
      }
    }
    return query;
  }
}
