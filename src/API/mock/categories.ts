export const categories: Array<string> = [
  "Кроссовки",
  "Носки",
  "Брюки",
  "Футболки",
  "Шапки",
  "Аксессуары",
];

interface IcategoriesURL {
  [key: string]: string;
}

export const categoriesURL: IcategoriesURL = {
  Кроссовки: "krossovki",
  Носки: "noski",
  Брюки: "bryuki",
  Футболки: "futbolki",
  Шапки: "shapki",
  Аксессуары: "aksessuary",
};
