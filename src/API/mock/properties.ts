export interface IShoeProperties {
  colors: Array<string>;
  sizes: Array<string>;
  brand: Array<string>;
}

export const shoeProperties: IShoeProperties = {
  colors: [
    "#FF0000",
    "#00FF75",
    "#FFBCD6",
    "#FFF",
    "#FAFF09",
    "#BF10EB",
    "#EE0F65",
    "#000",
    "#F9EEDF",
  ],
  sizes: ["37", "39", "40", "41", "42", "L", "XL", "XXL"],
  brand: ["Nike", "New Balance", "Adidas", "Asics"],
};

export interface ICommonProperties {
  gender: Array<string>;
  discount: Array<string>;
}

export const commonProperties: ICommonProperties = {
  gender: ["m", "f", "mf"],
  discount: ["withDiscount", "withoutDiscount", "all"],
};
