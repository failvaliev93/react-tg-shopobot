import { Product } from "../../types/Product";

export const mockProducts: Array<Product> = [
  {
    id: "1",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Nike",
    description: "Air Jordan 1 Retro High Obsidian UNC",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает.`,
    price: 16200,
    discount: 10,
    gender: "mf",
    sizes: ["37"],
    images: [
      "/img/products/shoe/1.png",
      "/img/products/shoe/1.png",
      "/img/products/shoe/1.png",
      "/img/products/shoe/1.png",
    ],
  },
  {
    id: "2",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Adidas",
    description: "Originals Ozweego trainers in meta",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 24900,
    gender: "m",
    sizes: ["37", "39", "40", "41", "42"],
    images: [
      "/img/products/shoe/2-adidas.png",
      "/img/products/shoe/2-adidas-1.png",
      "/img/products/shoe/2-adidas-2.png",
      "/img/products/shoe/2-adidas-3.png",
    ],
    color: "#F9EEDF",
    colors: ["#00FF75", "#F9EEDF", "#FFF"],
  },
  {
    id: "3",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Nike",
    description: "Air Force 1 Shadow Beige Pale Ivory",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 18900,
    discount: 40,
    gender: "f",
    sizes: ["37", "39", "40", "41", "42"],
    images: [
      "/img/products/shoe/3-nike.png",
      "/img/products/shoe/3-nike-1.png",
      "/img/products/shoe/3-nike-2.png",
      "/img/products/shoe/3-nike-3.png",
      "/img/products/shoe/3-nike-4.png",
      "/img/products/shoe/3-nike-4.png",
    ],
    color: "#FFBCD6",
    colors: ["#FF0000", "#00FF75", "#FFBCD6", "#FFF"],
  },
  {
    id: "4",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "New Balance",
    description: "Air Force 1 Jester XX Black Sonic Yellow",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 22900,
    gender: "m",
    sizes: ["40", "41", "42"],
    images: ["/img/products/shoe/4.png"],
  },
  {
    id: "5",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Nike",
    description: "Nike SB Dunk Low x Gratefu",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 180900,
    discount: 40,
    gender: "m",
    sizes: ["40", "41", "42"],
    images: [
      "/img/products/shoe/5.png",
      "/img/products/shoe/5.png",
      "/img/products/shoe/5.png",
      "/img/products/shoe/5.png",
      "/img/products/shoe/5.png",
    ],
  },
  {
    id: "6",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Asics",
    description: "Air Force 1 Jester XX Black Sonic Yellow",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 8900,
    gender: "mf",
    sizes: ["40", "41", "42"],
    images: ["/img/products/shoe/6.png"],
  },
  {
    id: "7",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Nike",
    description: "Air Jordan 1 Retro High Obsidian UNC",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает...
    Подробнее`,
    price: 24900,
    discount: 10,
    gender: "mf",
    sizes: ["37"],
    images: [
      "/img/products/shoe/1.png",
      "/img/products/shoe/1.png",
      "/img/products/shoe/1.png",
      "/img/products/shoe/1.png",
    ],
  },
  {
    id: "8",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Adidas",
    description: "Originals Ozweego trainers in meta",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 24900,
    gender: "m",
    sizes: ["37", "39", "40", "41", "42"],
    images: [
      "/img/products/shoe/2-adidas.png",
      "/img/products/shoe/2-adidas-1.png",
      "/img/products/shoe/2-adidas-2.png",
      "/img/products/shoe/2-adidas-3.png",
    ],
    color: "#F9EEDF",
    colors: ["#00FF75", "#F9EEDF", "#FFF"],
  },
  {
    id: "9",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Nike",
    description: "Air Force 1 Shadow Beige Pale Ivory",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 18900,
    discount: 40,
    gender: "f",
    sizes: ["37", "39", "40", "41", "42"],
    images: [
      "/img/products/shoe/3-nike.png",
      "/img/products/shoe/3-nike-1.png",
      "/img/products/shoe/3-nike-2.png",
      "/img/products/shoe/3-nike-3.png",
      "/img/products/shoe/3-nike-4.png",
      "/img/products/shoe/3-nike-4.png",
    ],
    color: "#FFBCD6",
    colors: ["#FF0000", "#00FF75", "#FFBCD6", "#FFF"],
  },
  {
    id: "10",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "New Balance",
    description: "Air Force 1 Jester XX Black Sonic Yellow",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 22900,
    gender: "m",
    sizes: ["40", "41", "42"],
    images: ["/img/products/shoe/4.png"],
  },
  {
    id: "11",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Nike",
    description: "Nike SB Dunk Low x Gratefu",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 180900,
    discount: 40,
    gender: "m",
    sizes: ["40", "41", "42"],
    images: [
      "/img/products/shoe/5.png",
      "/img/products/shoe/5.png",
      "/img/products/shoe/5.png",
      "/img/products/shoe/5.png",
      "/img/products/shoe/5.png",
    ],
  },
  {
    id: "12",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Asics",
    description: "Air Force 1 Jester XX Black Sonic Yellow",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 1337,
    gender: "mf",
    sizes: ["40", "41", "42"],
    images: ["/img/products/shoe/6.png"],
  },
  {
    id: "13",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Nike",
    description: "Air Jordan 1 Retro High Obsidian UNC",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает...
    Подробнее`,
    price: 16200,
    discount: 10,
    gender: "mf",
    sizes: ["37"],
    images: [
      "/img/products/shoe/1.png",
      "/img/products/shoe/1.png",
      "/img/products/shoe/1.png",
      "/img/products/shoe/1.png",
    ],
  },
  {
    id: "14",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Adidas",
    description: "Originals Ozweego trainers in meta",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 24900,
    gender: "m",
    sizes: ["37", "39", "40", "41", "42"],
    images: [
      "/img/products/shoe/2-adidas.png",
      "/img/products/shoe/2-adidas-1.png",
      "/img/products/shoe/2-adidas-2.png",
      "/img/products/shoe/2-adidas-3.png",
    ],
    color: "#F9EEDF",
    colors: ["#00FF75", "#F9EEDF", "#FFF"],
  },
  {
    id: "15",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Nike",
    description: "Air Force 1 Shadow Beige Pale Ivory",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 18900,
    discount: 40,
    gender: "f",
    sizes: ["37", "39", "40", "41", "42"],
    images: [
      "/img/products/shoe/3-nike.png",
      "/img/products/shoe/3-nike-1.png",
      "/img/products/shoe/3-nike-2.png",
      "/img/products/shoe/3-nike-3.png",
      "/img/products/shoe/3-nike-4.png",
      "/img/products/shoe/3-nike-4.png",
    ],
    color: "#FFBCD6",
    colors: ["#FF0000", "#00FF75", "#FFBCD6", "#FFF"],
  },
  {
    id: "16",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "New Balance",
    description: "Air Force 1 Jester XX Black Sonic Yellow",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 22900,
    gender: "m",
    sizes: ["40", "41", "42"],
    images: ["/img/products/shoe/4.png"],
  },
  {
    id: "17",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Nike",
    description: "Nike SB Dunk Low x Gratefu",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 180900,
    discount: 40,
    gender: "m",
    sizes: ["40", "41", "42"],
    images: [
      "/img/products/shoe/5.png",
      "/img/products/shoe/5.png",
      "/img/products/shoe/5.png",
      "/img/products/shoe/5.png",
      "/img/products/shoe/5.png",
    ],
  },
  {
    id: "18",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Asics",
    description: "Air Force 1 Jester XX Black Sonic Yellow",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 8900,
    gender: "mf",
    sizes: ["40", "41", "42"],
    images: ["/img/products/shoe/6.png"],
  },
  {
    id: "19",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Nike",
    description: "Air Jordan 1 Retro High Obsidian UNC",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает...
    Подробнее`,
    price: 24900,
    discount: 10,
    gender: "mf",
    sizes: ["37"],
    images: [
      "/img/products/shoe/1.png",
      "/img/products/shoe/1.png",
      "/img/products/shoe/1.png",
      "/img/products/shoe/1.png",
    ],
  },
  {
    id: "20",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Adidas",
    description: "Originals Ozweego trainers in meta",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 24900,
    gender: "m",
    sizes: ["37", "39", "40", "41", "42"],
    images: [
      "/img/products/shoe/2-adidas.png",
      "/img/products/shoe/2-adidas-1.png",
      "/img/products/shoe/2-adidas-2.png",
      "/img/products/shoe/2-adidas-3.png",
    ],
    color: "#F9EEDF",
    colors: ["#00FF75", "#F9EEDF", "#FFF"],
  },
  {
    id: "21",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Nike",
    description: "Air Force 1 Shadow Beige Pale Ivory",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 18900,
    discount: 40,
    gender: "f",
    sizes: ["37", "39", "40", "41", "42"],
    images: [
      "/img/products/shoe/3-nike.png",
      "/img/products/shoe/3-nike-1.png",
      "/img/products/shoe/3-nike-2.png",
      "/img/products/shoe/3-nike-3.png",
      "/img/products/shoe/3-nike-4.png",
      "/img/products/shoe/3-nike-4.png",
    ],
    color: "#FFBCD6",
    colors: ["#FF0000", "#00FF75", "#FFBCD6", "#FFF"],
  },
  {
    id: "22",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "New Balance",
    description: "Air Force 1 Jester XX Black Sonic Yellow",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 22900,
    gender: "m",
    sizes: ["40", "41", "42"],
    images: ["/img/products/shoe/4.png"],
  },
  {
    id: "23",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Nike",
    description: "Nike SB Dunk Low x Gratefu",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 180900,
    discount: 40,
    gender: "m",
    sizes: ["40", "41", "42"],
    images: [
      "/img/products/shoe/5.png",
      "/img/products/shoe/5.png",
      "/img/products/shoe/5.png",
      "/img/products/shoe/5.png",
      "/img/products/shoe/5.png",
    ],
  },
  {
    id: "24",
    type: {
      name: "Кроссовки",
      adres: "krossovki",
    },
    brand: "Asics",
    description: "Air Force 1 Jester XX Black Sonic Yellow",
    text: `Новая волна комфорта и стиля. Приподнятая подошва и многослойный верх напоминают о бунтарской андеграундной культуре.
    Многослойная конструкция придает легендарной баскетбольной модели уникальный вид, а роскошное сочетание мягкой синтетической замши и блестящей кожи создает. 
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet justo donec enim diam. Semper quis lectus nulla at volutpat diam. Risus viverra adipiscing at in tellus integer feugiat.`,
    price: 1337,
    gender: "mf",
    sizes: ["40", "41", "42"],
    images: ["/img/products/shoe/6.png"],
  },
];
