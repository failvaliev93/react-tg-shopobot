import { DiscountSelector, Gender, Product } from "../types/Product";
import { SortBy } from "../types/toolBar";
import { BaseAPI } from "./BaseAPI";

export interface fetchProductsParams {
  category?: string;
  properties?: {
    colors?: Array<string>;
    sizes?: Array<string>;
    discount?: DiscountSelector;
    gender?: Gender | "all";
    price?: {
      min: number;
      max: number;
    };
    sort?: SortBy;
  };
  search?: {
    word: string;
  };
  page?: number;
  limit?: number;
}

export interface IFetchProductReturn {
  list: Array<Product>;
  productsCount: number;
  maxPages: number;
  page: number;
  properties: IReturnProductProperties;
}

interface IReturnProductProperties {
  priceLimitMin: number;
  priceLimitMax: number;
  sizesAll: Array<string>;
  colorsAll: Array<string>;
}

export class ProductsAPI {
  static async get(
    queryParams: fetchProductsParams
  ): Promise<IFetchProductReturn> {
    const { data } = await BaseAPI.get("/products", queryParams);
    return data;
  }

  static async getOne(id: string): Promise<IFetchProductReturn> {
    const { data } = await BaseAPI.get("/product", { id });
    return data;
  }

  static async getFilter(category: string): Promise<fetchProductsParams> {
    const { data } = await BaseAPI.get("/product/filter", { category });
    return data;
  }
}
