import { BaseAPI } from "./BaseAPI";

export interface ICategory {
  name: string;
  adres: string;
}

export class CategoriesAPI {
  static async getAll(): Promise<Array<ICategory>> {
    const { data } = await BaseAPI.get("/categories");
    const categories: Array<ICategory> = [];

    for (const key in data) {
      if (Object.prototype.hasOwnProperty.call(data, key)) {
        categories.push({
          name: key,
          adres: data[key],
        });
      }
    }

    return categories;
  }
}
