import React from "react";
import { Snackbar, Alert } from "@mui/material";
import { RemindIcon } from "../icons/RemindIcon";

interface INotifyBottom {
  text: string;
  open: boolean;
  handleClose: () => void;
}

export const NotifyBottom = ({
  text,
  handleClose,
  ...props
}: INotifyBottom) => {
  return (
    <Snackbar
      open={props.open}
      autoHideDuration={6000}
      onClose={handleClose}
      anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
    >
      <Alert onClose={handleClose} severity="warning" icon={<RemindIcon />}>
        {text}
      </Alert>
    </Snackbar>
  );
};
