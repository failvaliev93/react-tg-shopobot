import { SxProps, Theme, ToggleButton } from "@mui/material";
import classNames from "classnames";
import React from "react";
import s from "./SizeToggleButton.module.css";

interface SizeToggleButtonProps {
  onChange: (selected: boolean) => void;
  size: string;
  disabled?: boolean;
  selected?: boolean;
  sx?: SxProps<Theme> | undefined;
}

const SizeToggleButton = ({
  size,
  onChange,
  selected,
  disabled,
  sx,
}: SizeToggleButtonProps) => {
  const onClick = () => {
    onChange(!selected);
  };

  return (
    <div
      className={
        selected ? classNames(s.sizeToggleButton, s.active) : s.sizeToggleButton
      }
    >
      <ToggleButton
        value="check"
        selected={selected}
        onChange={onClick}
        disabled={disabled}
        size="small"
        sx={{
          "&": {
            outline: selected ? "2px solid #000" : "initial",
            borderRadius: "7px",
            color: "#000",
            padding: "7px 16.5px",
            fontSize: "16px",
          },
          "&:hover": {
            backgroundColor: "initial",
          },
          "&.Mui-selected": {
            backgroundColor: "initial !important",
          },
          ...sx,
        }}
      >
        {size}
      </ToggleButton>
    </div>
  );
};

export default SizeToggleButton;
