import React from "react";
import RemoveIcon from "@mui/icons-material/Remove";
import { RoundedButton } from "./RoundedButton";

interface IDecButton {
  onClick: () => void;
  disabled?: boolean;
}

export const DecButton = ({ onClick, disabled }: IDecButton) => {
  return (
    <RoundedButton onClick={onClick} disabled={disabled}>
      <RemoveIcon color={disabled ? "disabled" : "success"} />
    </RoundedButton>
  );
};
