import React from "react";
import { IconButton } from "@mui/material";

interface IIncButton {
  onClick: () => void;
  children: React.ReactNode;
  disabled?: boolean;
}

export const RoundedButton = ({ onClick, children, disabled }: IIncButton) => {
  return (
    <div>
      <IconButton
        size="small"
        onClick={onClick}
        sx={{
          paddingLeft: "0",
          borderRadius: "17px",
          border: "1px solid #F0F0F0",
          width: "41px",
          height: "41px",
          padding: "5px",
        }}
        disabled={disabled}
      >
        {children}
      </IconButton>
    </div>
  );
};
