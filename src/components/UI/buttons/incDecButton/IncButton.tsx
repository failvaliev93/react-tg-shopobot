import React from "react";
import AddIcon from "@mui/icons-material/Add";
import { RoundedButton } from "./RoundedButton";

interface IIncButton {
  onClick: () => void;
  disabled?: boolean;
}

export const IncButton = ({ onClick, disabled }: IIncButton) => {
  return (
    <RoundedButton onClick={onClick} disabled={disabled}>
      <AddIcon color={disabled ? "disabled" : "success"} />
    </RoundedButton>
  );
};
