import { IconButton } from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";
import { ArrowNavLeftIcon } from "../../icons/ArrowNavLeftIcon";
import s from "./BackButton.module.css";

export const BackButton = () => {
  const navigate = useNavigate();

  const goBack = () => navigate(-1);

  return (
    <div className={s.back}>
      <IconButton
        size="small"
        onClick={goBack}
        sx={{
          paddingLeft: "0",
        }}
      >
        <ArrowNavLeftIcon />
      </IconButton>
    </div>
  );
};
