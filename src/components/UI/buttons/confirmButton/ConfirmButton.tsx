import { Button } from "@mui/material";
import React from "react";

interface IConfirmButton {
  text: string;
  onClick: () => void;
}

export const ConfirmButton = ({ text, onClick }: IConfirmButton) => {
  return (
    <Button
      color="success"
      size="large"
      variant="contained"
      fullWidth
      onClick={() => onClick()}
    >
      {text}
    </Button>
  );
};
