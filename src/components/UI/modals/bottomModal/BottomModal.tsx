import { Modal } from "@mui/material";
import React from "react";
import s from "./BottomModal.module.css";

interface BottomWindowProps {
  onClose: () => void;
  name: string;
  children?: React.ReactNode;
}

const BottomModal = (props: BottomWindowProps) => {
  const closeHandler = () => props.onClose();
  const windowHandler = (e: React.SyntheticEvent) => e.stopPropagation();

  return (
    <Modal
      open={true}
      onClose={closeHandler}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <div className={s.window} onClick={windowHandler}>
        <div className={s.header}>
          <span>{props.name}</span>
          <img
            src="/img/icons/close.svg"
            alt="close"
            onClick={closeHandler}
            className={s.close}
          />
        </div>
        <div className={s.content}>{props.children || null}</div>
      </div>
    </Modal>
  );
};

export default BottomModal;
