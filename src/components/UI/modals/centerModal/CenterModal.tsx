import { Modal } from "@mui/material";
import React from "react";
import s from "./CenterModal.module.css";

interface ICenterModal {
  onClose: () => void;
  children?: React.ReactNode;
}

export const CenterModal = (props: ICenterModal) => {
  const closeHandler = () => props.onClose();
  const windowHandler = (e: React.SyntheticEvent) => e.stopPropagation();

  return (
    <Modal
      open={true}
      onClose={closeHandler}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <div className={s.wrapper}>
        <div className={s.window} onClick={windowHandler}>
          <div className={s.content}>{props.children}</div>
        </div>
      </div>
    </Modal>
  );
};
