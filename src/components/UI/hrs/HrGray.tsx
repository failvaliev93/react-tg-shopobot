import { styled, SxProps, Theme } from "@mui/material";
import React from "react";

const HrStyled = styled("hr")(({ theme }) => ({
  color: "#d8d8dd",
  opacity: "0.5",
}));

interface IHrGray {
  sx?: SxProps<Theme> | undefined;
}

export const HrGray = (props: IHrGray) => {
  return <HrStyled {...props} />;
};
