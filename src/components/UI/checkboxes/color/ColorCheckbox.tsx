import classNames from "classnames";
import { styled, SxProps, Theme } from "@mui/material";
import React from "react";
import s from "./ColorCheckbox.module.css";

const Styled = styled("div")(({ theme }) => ({}));

interface ColorCheckboxProps {
  color: string;
  onChange: (checked: boolean) => void;
  checked?: boolean;
  sx?: SxProps<Theme> | undefined;
}

const ColorCheckbox = (props: ColorCheckboxProps) => {
  const onclick = () => props.onChange(!props.checked);

  return (
    <Styled
      className={props.checked ? classNames(s.color, s.active) : s.color}
      onClick={onclick}
      sx={{
        backgroundColor: props.color,
        ...props.sx,
      }}
    ></Styled>
  );
};

export default ColorCheckbox;
