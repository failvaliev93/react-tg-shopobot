import React from "react";
import { styled, alpha, SxProps } from "@mui/material/styles";
import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";
import { Theme } from "@mui/material";
import { useDebouncedCallback } from "use-debounce";
import { inputDelay } from "../../../../../consts/setup";

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.black, 0.06),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.black, 0.04),
  },
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(1),
    // width: "auto",
  },
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  width: "100%",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      // width: "12ch",
      "&:focus": {
        // width: "20ch",
      },
    },
  },
}));

const SearchIconWrapped = styled(SearchIcon)(({ theme }) => ({
  color: "#8B8B8B",
}));

interface ProductSearchProps {
  onSearch: (searchValue: string) => void;
  sx?: SxProps<Theme> | undefined;
}

export const ProductSearch = (props: ProductSearchProps) => {
  const [searchValue, setSearchValue] = React.useState<string>("");

  const debouncedSearch = useDebouncedCallback(
    (searchValue) => props.onSearch(searchValue),
    inputDelay
  );

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchValue(e.target.value);
    debouncedSearch(e.target.value);
  };

  return (
    <Search sx={props.sx}>
      <SearchIconWrapper>
        <SearchIconWrapped />
      </SearchIconWrapper>
      <StyledInputBase
        placeholder="Поиск…"
        inputProps={{ "aria-label": "search" }}
        onChange={onChange}
        value={searchValue}
      />
    </Search>
  );
};
