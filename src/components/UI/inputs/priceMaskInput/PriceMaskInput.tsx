import { TextField } from "@mui/material";
import React from "react";
import NumberFormat, { InputAttributes } from "react-number-format";

interface CustomProps {
  onChange: (event: { target: { name: string; value: string } }) => void;
  name: string;
}

const NumberFormatCustom = React.forwardRef<
  NumberFormat<InputAttributes>,
  CustomProps
>(function NumberFormatCustom(props, ref) {
  const { onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={ref}
      onValueChange={(values) => {
        onChange({
          target: {
            name: props.name,
            value: values.value,
          },
        });
      }}
      thousandSeparator=" "
      isNumericString
      // prefix="$"
    />
  );
});

interface PriceMaskInputProps {
  value: any;
  onChange: React.ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>;
}

const PriceMaskInput = ({ value, onChange, ...other }: PriceMaskInputProps) => {
  return (
    <TextField
      value={value}
      onChange={onChange}
      InputProps={{
        inputComponent: NumberFormatCustom as any,
      }}
      size="small"
      {...other}
    />
  );
};

export default PriceMaskInput;
