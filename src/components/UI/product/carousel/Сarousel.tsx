import React from "react";
import s from "./Сarousel.module.css";

interface ICarousel {
  imagesURL: Array<string>;
}

export const Сarousel = (props: ICarousel) => {
  const [activeURL, setActiveURL] = React.useState<string>(props.imagesURL[0]);

  const onClick = (url: string) => {
    if (activeURL !== url) {
      setActiveURL(url);
    }
  };

  return (
    <div className={s.carousel}>
      <div className={s.preview}>
        <img src={activeURL} alt="product view" />
      </div>
      <div className={s.buttons}>
        {props.imagesURL.map((url, i) => (
          <div
            className={activeURL !== url ? s.button : `${s.button} ${s.active}`}
            key={url + i}
            onClick={() => onClick(url)}
          >
            <img src={url} alt="product preview" />
          </div>
        ))}
      </div>
    </div>
  );
};
