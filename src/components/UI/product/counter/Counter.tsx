import { Grid, Typography, Theme } from "@mui/material";
import { SxProps } from "@mui/material/styles";
import React from "react";
import { DecButton } from "../../buttons/incDecButton/DecButton";
import { IncButton } from "../../buttons/incDecButton/IncButton";

interface ICounter {
  onClick: (value: number) => void;
  min: number;
  max: number;
  countValue: number;
  sx?: SxProps<Theme> | undefined;
}

const CounterComponent = ({
  onClick,
  countValue,
  min,
  max,
  sx = {
    fontSize: "20px",
    fontWeight: 500,
    lineHeight: "24px",
  },
}: ICounter) => {
  const incrementHandler = () => onClick(countValue + 1);
  const decrementHandler = () => onClick(countValue - 1);

  return (
    <Grid
      item
      container
      direction="row"
      wrap="nowrap"
      columns={3}
      spacing={2}
      justifyContent="space-between"
    >
      <Grid item>
        <DecButton onClick={decrementHandler} disabled={countValue === min} />
      </Grid>
      <Grid item alignItems="center" container justifyContent="center">
        <Typography sx={sx}>{countValue}</Typography>
      </Grid>
      <Grid item>
        <IncButton onClick={incrementHandler} disabled={countValue === max} />
      </Grid>
    </Grid>
  );
};

export const Counter = React.memo(CounterComponent);
