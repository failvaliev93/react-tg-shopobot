import React from "react";
import {
  addFavoriteAC,
  delFavoriteAC,
} from "../../../../features/shop/shopSlice";
import { useFavorites } from "../../../../hooks/useFavorites";
import { useAppDispatch } from "../../../../redux/hooks";
import { Product } from "../../../../types/Product";
import Favorite from "./Favorite";

interface IFavoriteWithHandler {
  product: Product;
}

const FavoriteWithHandlerComponent = ({ product }: IFavoriteWithHandler) => {
  const dispatch = useAppDispatch();

  const [activeFavorite, setActiveFavorite] = React.useState<boolean>(false);
  const allFavorites = useFavorites();

  React.useEffect(() => {
    if (allFavorites !== null) {
      const res = allFavorites.find((prod) => product.id === prod.id);
      if (res) {
        setActiveFavorite(true);
      }
    }
  }, [product, allFavorites]);

  const favoriteHandler = (active: boolean) => {
    if (active) {
      dispatch(addFavoriteAC(product));
    } else {
      dispatch(delFavoriteAC(product));
    }
    setActiveFavorite(active);
  };

  return <Favorite onClick={favoriteHandler} active={activeFavorite} />;
};

export const FavoriteWithHandler = React.memo(FavoriteWithHandlerComponent);
