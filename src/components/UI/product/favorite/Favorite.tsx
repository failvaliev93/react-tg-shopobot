import React, { useState } from "react";
import s from "./Favorite.module.css";

interface FavoriteProps {
  active?: boolean;
  onClick?: (active: boolean) => void;
}

const Favorite = ({ onClick = () => {}, active, ...props }: FavoriteProps) => {
  const handleClick = () => {
    onClick(!active);
  };

  return (
    <div className={s.favoriteBtn} onClick={() => handleClick()}>
      {active ? (
        <img src="/img/icons/favs.svg" alt="Favorite" />
      ) : (
        <img src="/img/icons/favs2.svg" alt="Not favorite" />
      )}
    </div>
  );
};

export default Favorite;
