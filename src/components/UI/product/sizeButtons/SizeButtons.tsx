import { SxProps, Theme } from "@mui/material";
import React from "react";
import SizeToggleButton from "../../buttons/sizeToggleButton/SizeToggleButton";
import s from "./SizeButtons.module.css";

interface ISizeButtons {
  sizes: Array<string>;
  onChange: (selected: boolean, size: string) => void;
  selected?: Array<string>;
  enabled?: Array<string>;
  sx?: SxProps<Theme> | undefined;
}

export const SizeButtons = (props: ISizeButtons) => {
  return (
    <div className={s.sizes}>
      {props.sizes.map((size) => (
        <SizeToggleButton
          key={size}
          size={size}
          onChange={(selected) => props.onChange(selected, size)}
          selected={props?.selected && props?.selected.includes(size)}
          disabled={props?.enabled && !props?.enabled.includes(size)}
          sx={props.sx}
        />
      ))}
    </div>
  );
};
