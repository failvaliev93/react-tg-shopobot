import React from "react";
import { SizeButtons } from "./SizeButtons";

interface ISizeButtonsOneSelect {
  onSelect: (size: string) => void;
  sizes: Array<string>;
  selectedSize?: string;
  enabled?: Array<string>;
}

export const SizeButtonsOneSelect = (props: ISizeButtonsOneSelect) => {
  const [selectedSizes, setSelectedSizes] = React.useState<Array<string>>(
    props.selectedSize ? [props.selectedSize] : []
  );

  const changeHandler = (sel: boolean, size: string) => {
    if (sel) {
      setSelectedSizes([size]);
      props.onSelect(size);
    }
  };

  return (
    <SizeButtons
      sizes={props.sizes}
      onChange={changeHandler}
      enabled={props.enabled}
      selected={selectedSizes}
    />
  );
};
