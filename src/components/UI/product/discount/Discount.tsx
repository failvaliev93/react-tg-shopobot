import React from "react";
import s from "./Discount.module.css";

interface DiscountProps {
  perCount: number;
}

const Discount = ({ perCount }: DiscountProps) => {
  return <span className={s.discountChip}>{perCount}%</span>;
};

export default Discount;
