import { styled, SxProps } from "@mui/material/styles";
import { Theme } from "@mui/material";
import React from "react";
import s from "./Price.module.css";

const PriceStyled = styled("span")(({ theme }) => ({
  fontSize: "14px",
  fontWeight: 500,
  lineHeight: "17px",
}));

interface PriceProps {
  value: number;
  discount?: number | undefined;
  sx?: SxProps<Theme> | undefined;
}

const Price = ({ value, discount, sx }: PriceProps) => {
  const discounted = discount ? (
    <span className={s.discounted}>
      <span className={s.discountedlinethrough}>
        {value + (discount / 100) * value}
      </span>
      {""}₽
    </span>
  ) : null;

  return (
    <PriceStyled sx={sx}>
      {discounted}
      <span className={s.priceValue}>{value}₽</span>
    </PriceStyled>
  );
};

export default Price;
