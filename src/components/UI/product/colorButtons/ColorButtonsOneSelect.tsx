import React from "react";
import { ColorButtons } from "./ColorButtons";

interface IColorButtonsOneSelect {
  onSelect: (size: string) => void;
  colors: Array<string>;
  selectedColor: string;
}

export const ColorButtonsOneSelect = (props: IColorButtonsOneSelect) => {
  const [selectedColors, setSelectedColors] = React.useState<Array<string>>(
    props.selectedColor ? [props.selectedColor] : []
  );

  const selectHandler = (sel: boolean, color: string) => {
    if (sel) {
      setSelectedColors([color]);
      props.onSelect(color);
    }
  };

  return (
    <ColorButtons
      values={props.colors}
      onChange={selectHandler}
      checked={selectedColors}
      sx={{
        width: "40px",
        height: "40px",
        margin: "0",
      }}
    />
  );
};
