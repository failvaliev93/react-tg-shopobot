import React from "react";
import { SxProps, Theme } from "@mui/material";
import ColorCheckbox from "../../checkboxes/color/ColorCheckbox";
import s from "./ColorButtons.module.css";

interface IColorButtons {
  values: Array<string>;
  checked: Array<string>;
  onChange: (checked: boolean, color: string) => void;
  sx?: SxProps<Theme> | undefined;
}

export const ColorButtons = (props: IColorButtons) => {
  return (
    <div className={s.colors}>
      {props.values.map((color, i) => (
        <ColorCheckbox
          key={i}
          color={color}
          checked={props.checked.includes(color)}
          onChange={(checked) => props.onChange(checked, color)}
          sx={props.sx}
        />
      ))}
    </div>
  );
};
