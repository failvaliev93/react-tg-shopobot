import { styled, SxProps } from "@mui/material/styles";
import { Theme } from "@mui/material";
import React from "react";
import { useToggle } from "../../../../hooks/useToggle";
import s from "./SemiHiddenText.module.css";

const TextStyled = styled("p")(() => ({
  fontSize: "16px",
  fontWeight: 400,
  lineHeight: "20px",
  maxHeight: "10rem",
  overflow: "hidden",
}));

interface ISemiHiddenText {
  text: string;
  sx?: SxProps<Theme> | undefined;
}

export const SemiHiddenText = ({ text, sx }: ISemiHiddenText) => {
  const [show, setShow] = useToggle(false);

  return (
    <div>
      <TextStyled sx={sx} className={show ? s.fullText : ""}>
        {text}
      </TextStyled>
      <p className={s.more} onClick={() => setShow()}>
        {!show ? "Подробнее" : "Скрыть"}
      </p>
    </div>
  );
};
