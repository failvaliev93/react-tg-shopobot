import React from "react";

export const RemindIcon = () => (
  <img src="/img/icons/remind.svg" alt="remind" width={19} height={19} />
);
