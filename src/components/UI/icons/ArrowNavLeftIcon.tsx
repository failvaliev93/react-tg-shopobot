import React from "react";

export const ArrowNavLeftIcon = () => (
  <img src="/img/icons/arrow-nav-left.svg" alt="arrow navigation back" />
);
