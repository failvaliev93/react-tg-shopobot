import React from "react";

export const ArrowNavRightIcon = () => (
  <img src="/img/icons/arrow-nav-right.svg" alt="arrow navigation right" />
);
