import { Grid, Typography } from "@mui/material";
import React from "react";
import { BackButton } from "../../../UI/buttons/backButton/BackButton";
import { IPage } from "../Page";

interface IMobile extends IPage {}

export const Mobile = (props: IMobile) => {
  return (
    <>
      {(props.pageName || props.backButton) && (
        <Grid
          container
          spacing={1}
          sx={{
            "&": {
              marginBottom: "1.1rem",
              marginTop: ".5rem",
            },
          }}
        >
          {props.backButton && (
            <Grid item>
              <BackButton />
            </Grid>
          )}
          {props.pageName && (
            <Grid item>
              <Typography variant="h4">{props.pageName}</Typography>
            </Grid>
          )}
        </Grid>
      )}
      {props.children}
    </>
  );
};
