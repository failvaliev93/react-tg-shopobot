import React from "react";
import { Mobile } from "./mobile/Mobile";

export interface IPage {
  backButton?: boolean;
  pageName?: string;
  children?: React.ReactNode;
}

export const Page = (props: IPage) => {
  return (
    <>
      <Mobile {...props} />
    </>
  );
};
