import React from "react";
import ShopPage from "../../../pages/shopPage/ShopPage";
import s from "./Content.module.css";
import { SearchPage } from "../../../pages/searchPage/SearchPage";
import { Routes, Route } from "react-router-dom";
import { CategoryPage } from "../../../pages/categoryPage/CategoryPage";
import { ProductPage } from "../../../pages/productPage/ProductPage";
import { FavoritePage } from "../../../pages/favoritePage/FavoritePage";
import { BasketPage } from "../../../pages/basketPage/BasketPage";

export function Content() {
  return (
    <div className={s.content}>
      <Routes>
        <Route path="/" element={<ShopPage />} />
        <Route path="/catalog" element={<SearchPage />} />
        <Route path="/catalog/:category" element={<CategoryPage />} />
        <Route path="/catalog/:category/:id" element={<ProductPage />} />
        <Route path="/favorite" element={<FavoritePage />} />
        <Route path="/basket" element={<BasketPage />} />
      </Routes>
    </div>
  );
}
