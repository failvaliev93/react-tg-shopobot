import classNames from "classnames";
import React, { useState } from "react";
import s from "./Banner.module.css";

export const Banner = () => {
  const [activeSlide, setActiveSlide] = useState(0);

  const bannerImgs = [
    "img/banner/banner1.png",
    "img/banner/banner2.png",
    "img/banner/banner3.png",
  ];

  const bannerSelectItems = bannerImgs.map((_, i) => (
    <div
      className={
        activeSlide === i
          ? classNames(s.bannerSelectItem, s.bannerSelectItemActiv)
          : s.bannerSelectItem
      }
      key={i}
      onClick={(e) => setActiveSlide(i)}
    />
  ));
  const bannerSelect =
    bannerImgs.length > 1 ? (
      <div className={s.bannerSelect}>{bannerSelectItems}</div>
    ) : null;

  const banners = bannerImgs.map((src, i) => (
    <img
      className={
        activeSlide === i
          ? classNames(s.bannerImg, s.bannerImgActive)
          : s.bannerImg
      }
      src={src}
      key={i}
      alt="banner"
    />
  ));

  return (
    <div className={s.banner}>
      {banners}
      {bannerSelect}
    </div>
  );
};
