import { Container, Typography } from "@mui/material";
import React from "react";
import { ConfirmButton } from "../../../../UI/buttons/confirmButton/ConfirmButton";
import { CenterModal } from "../../../../UI/modals/centerModal/CenterModal";

interface IOrderModal {
  onClose: () => void;
}

export const OrderModal = ({ onClose }: IOrderModal) => {
  return (
    <CenterModal onClose={onClose}>
      <Container
        sx={{
          display: "flex",
          justifyContent: "center",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <img src="/img/icons/confirm.svg" alt="confirm image" />
        <Typography
          variant="h6"
          sx={{
            paddingTop: "10px",
          }}
        >
          Заказ сформирован
        </Typography>
        <Typography
          variant="caption"
          sx={{
            paddingBottom: "20px",
          }}
        >
          Заказ сформирован
        </Typography>
        <ConfirmButton text="Закрыть" onClick={onClose} />
      </Container>
    </CenterModal>
  );
};
