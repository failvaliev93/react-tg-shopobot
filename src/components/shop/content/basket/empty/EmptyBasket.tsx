import React from "react";
import { Product } from "../../../../../types/Product";
import { SmallView } from "../../products/ProductList/listViews/SmallView";
import s from "./EmptyBasket.module.css";

interface IEmptyBasket {
  recomendatedProducts?: Array<Product>;
}

export const EmptyBasket = ({ recomendatedProducts }: IEmptyBasket) => {
  return (
    <div>
      <div className={s.notFound}>
        <img src="/img/icons/Basket-big.svg" alt="Basket" />
        <p>Ваша корзина пуста</p>
      </div>
      {recomendatedProducts && (
        <>
          <div className={s.interest}>
            <p>Возможно вас заинтересует</p>
          </div>
          <SmallView products={recomendatedProducts} />
        </>
      )}
    </div>
  );
};
