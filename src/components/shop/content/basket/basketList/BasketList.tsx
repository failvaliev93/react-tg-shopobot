import React from "react";
import { BasketProduct } from "../../../../../API/BasketAPI";
import { BasketItem } from "../basketItem/BasketItem";

interface IBasketList {
  items: Array<BasketProduct>;
  onDelete: (BProduct: BasketProduct) => void;
  onCountChange: (BProduct: BasketProduct, newCount: number) => void;
}

export const BasketList = ({ items, onDelete, onCountChange }: IBasketList) => {
  return (
    <>
      {items.map((bProd) => (
        <BasketItem
          item={bProd}
          onDelete={onDelete}
          onCountChange={onCountChange}
          key={bProd.product.id}
        />
      ))}
    </>
  );
};
