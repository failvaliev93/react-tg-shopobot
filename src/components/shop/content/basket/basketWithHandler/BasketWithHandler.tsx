import React from "react";
import { BasketProduct } from "../../../../../API/BasketAPI";
import { useBasket } from "../../../../../hooks/useBasket";
import { useAppDispatch } from "../../../../../redux/hooks";
import { BasketList } from "../basketList/BasketList";
import {
  delBasketAC,
  setBasketAC,
  updateBasketAC,
} from "../../../../../features/shop/shopSlice";
import { BasketLayout } from "../basketLayout/BasketLayout";
import { ConfirmButton } from "../../../../UI/buttons/confirmButton/ConfirmButton";
import { OrderModal } from "../orderModal/OrderModal";
import { PROMOCODE } from "../../../../../consts/setup";
import { EmptyBasket } from "../empty/EmptyBasket";

const calcSum = (basketList: Array<BasketProduct>): number => {
  if (!basketList.length) return 0;
  return basketList
    .map((item) => item.count * item.product.price)
    .reduce((part, cur) => part + cur);
};

export const BasketWithHandler = () => {
  const dispatch = useAppDispatch();
  const basket = useBasket();

  const delivery = 700;

  const [comment, setComment] = React.useState("");
  const [promocode, setPromocode] = React.useState("");
  const [discount, setDiscount] = React.useState(0);
  const [total, setTotal] = React.useState(0);
  const [openModal, setOpenModal] = React.useState(false);

  React.useEffect(() => {
    const sum = calcSum(basket || []);
    const discounted = (sum * (100 - discount)) / 100;
    const curTotal = delivery + discounted;
    setTotal(curTotal);
  }, [delivery, discount, basket]);

  const countHandler = (BProduct: BasketProduct, newCount: number) => {
    dispatch(updateBasketAC({ ...BProduct, count: newCount }));
  };
  const deleteHandler = (BProduct: BasketProduct) => {
    dispatch(delBasketAC(BProduct));
  };
  const promocodeHandler = (val: string) => {
    const value = val.toUpperCase();
    setPromocode(value);
    if (value === PROMOCODE) {
      setDiscount(15);
    }
    if (discount && value !== PROMOCODE) {
      setDiscount(0);
    }
  };
  const ConfirmButtonHandler = () => {
    setOpenModal(!openModal);
  };
  const modalCloseHandler = () => {
    setOpenModal(!openModal);
    dispatch(setBasketAC([]));
  };

  if (!basket?.length) {
    return <EmptyBasket />;
  }

  return (
    <>
      <BasketList
        items={basket || []}
        onCountChange={countHandler}
        onDelete={deleteHandler}
      />
      <BasketLayout>
        <BasketLayout.Comment text={comment} onChange={setComment} />
        <BasketLayout.Promocode
          promocode={promocode}
          onChange={promocodeHandler}
        />
        {discount > 0 && <BasketLayout.Discount discount={discount} />}
        <BasketLayout.Delivery delivery={delivery} />
        <BasketLayout.Total total={total} />
      </BasketLayout>
      <ConfirmButton
        text={`Оплатить ${total} ₽`}
        onClick={ConfirmButtonHandler}
      />
      {openModal && <OrderModal onClose={modalCloseHandler} />}
      <div />
    </>
  );
};
