import React from "react";
import { BasketProduct } from "../../../../../API/BasketAPI";
import { BasketProduct as BasketProductComp } from "./basketProduct/BasketProduct";

interface IBasketItem {
  item: BasketProduct;
  onDelete: (BProduct: BasketProduct) => void;
  onCountChange: (BProduct: BasketProduct, newCount: number) => void;
}

export const BasketItem = ({ item, onDelete, onCountChange }: IBasketItem) => {
  const deleteHandler = () => onDelete(item);

  const priceCounterHandler = (priceValue: number, countValue: number) => {
    onCountChange(item, countValue);
  };

  return (
    <div>
      <BasketProductComp
        imgSrc={item.product.images[0]}
        deleteComponent={<BasketProductComp.Delete onClick={deleteHandler} />}
        brandAndDescComponent={
          <BasketProductComp.BrandAndDesc
            brand={item.product.brand}
            description={item.product.description}
          />
        }
        priceWidthCounterComponent={
          <BasketProductComp.PriceCounter
            onClick={priceCounterHandler}
            priceValue={item.product.price}
            countValue={item.count}
          />
        }
      />
    </div>
  );
};
