import { Grid } from "@mui/material";
import React from "react";
import Price from "../../../../../../UI/product/price/Price";
import { Counter } from "../../../../../../UI/product/counter/Counter";

interface IPriceCounter {
  onClick: (priceValue: number, countValue: number) => void;
  priceValue: number;
  countValue: number;
}

export const PriceCounter = ({
  onClick,
  priceValue,
  countValue,
}: IPriceCounter) => {
  const counterHandler = (count: number) => {
    const newPice = count * priceValue;
    onClick(newPice, count);
  };

  return (
    <Grid container wrap="nowrap" spacing={1} item>
      <Grid item>
        <Counter
          onClick={counterHandler}
          min={1}
          max={100}
          countValue={countValue}
        />
      </Grid>
      <Grid item alignItems="center" wrap="nowrap" container>
        <Price
          value={priceValue * countValue}
          sx={{
            fontSize: "20px",
            fontWeight: 500,
            lineHeight: "24px",
          }}
        />
      </Grid>
    </Grid>
  );
};
