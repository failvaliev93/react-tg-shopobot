import { IconButton } from "@mui/material";
import React from "react";
import { TrashIcon } from "../../../../../../UI/icons/TrashIcon";

interface IDelete {
  onClick: () => void;
}

export const Delete = ({ onClick }: IDelete) => {
  return (
    <IconButton
      size="small"
      onClick={onClick}
      sx={{
        paddingLeft: "0",
      }}
    >
      <TrashIcon />
    </IconButton>
  );
};
