import { IconButton } from "@mui/material";
import React from "react";
import { BasketIcon } from "../../../../../../UI/icons/BasketIcon";

interface IBuy {
  onClick: () => void;
  bought?: boolean;
}

export const Buy = ({ onClick, bought }: IBuy) => {
  return (
    <IconButton
      size="small"
      onClick={onClick}
      sx={{
        paddingLeft: "0",
      }}
    >
      <BasketIcon color={bought ? "#46A758" : "#B3B3B3"} />
    </IconButton>
  );
};
