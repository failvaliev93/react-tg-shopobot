import { Grid, Typography } from "@mui/material";
import React from "react";

interface IProductBrandAndDesc {
  brand: string;
  description: string;
}

export const ProductBrandAndDesc = React.memo(
  ({ brand, description }: IProductBrandAndDesc) => {
    return (
      <Grid direction="column" container item>
        <Grid item>
          <Typography>{brand}</Typography>
        </Grid>
        <Grid item>
          <Typography variant="subtitle2">{description}</Typography>
        </Grid>
      </Grid>
    );
  }
);
