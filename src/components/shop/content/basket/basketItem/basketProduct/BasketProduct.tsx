import { Grid } from "@mui/material";
import { styled } from "@mui/material/styles";
import React from "react";
import { HrGray } from "../../../../../UI/hrs/HrGray";
import Price from "../../../../../UI/product/price/Price";
import { Buy } from "./components/Buy";
import { Delete } from "./components/Delete";
import { PriceCounter } from "./components/PriceCounter";
import { ProductBrandAndDesc } from "./components/ProductBrandAndDesc";

const ImgStyled = styled("img")(({ theme }) => ({
  width: "100%",
}));

interface IBasketProduct {
  imgSrc: string;
  brandAndDescComponent?: React.ReactNode;
  deleteComponent?: React.ReactNode;
  buyComponent?: React.ReactNode;
  priceComponent?: React.ReactNode;
  priceWidthCounterComponent?: React.ReactNode;
}

const BasketProduct = ({
  imgSrc,
  brandAndDescComponent,
  deleteComponent,
  buyComponent,
  priceComponent,
  priceWidthCounterComponent,
}: IBasketProduct) => {
  return (
    <div>
      <Grid
        direction="row"
        container
        spacing={1}
        justifyContent="space-between"
        columns={2}
        wrap="nowrap"
      >
        <Grid //2 колонки
          item
          direction="row"
          container
          spacing={1}
          columns={2}
          justifyContent="space-between"
          wrap="nowrap"
        >
          <Grid //img
            item
            sx={{
              width: "50%",
            }}
          >
            <ImgStyled src={imgSrc} alt="product preview" />
          </Grid>
          <Grid //text & delete & price
            item
            direction="column"
            container
            spacing={1}
            justifyContent="space-between"
            wrap="nowrap"
            sx={{
              height: "100%",
            }}
          >
            <Grid //2 колонки text & delete
              item
              direction="row"
              container
              spacing={1}
              columns={2}
              justifyContent="space-between"
              wrap="nowrap"
              sx={{
                height: "100%",
              }}
            >
              <Grid item>
                <Grid direction="column" container spacing={1}>
                  {brandAndDescComponent && (
                    <Grid item>{brandAndDescComponent}</Grid>
                  )}
                  {priceComponent && <Grid item>{priceComponent}</Grid>}
                </Grid>
              </Grid>
              {(deleteComponent || buyComponent) && (
                <Grid item>
                  <Grid //trash
                    item
                    direction="column"
                    container
                    wrap="nowrap"
                    justifyContent="space-between"
                    alignItems="center"
                    sx={{
                      height: "100%",
                    }}
                  >
                    {deleteComponent}
                    {buyComponent}
                  </Grid>
                </Grid>
              )}
            </Grid>
            {priceWidthCounterComponent && (
              <Grid
                item
                container
                justifyContent="center"
                direction="row"
                wrap="nowrap"
              >
                {priceWidthCounterComponent}
              </Grid>
            )}
          </Grid>
        </Grid>
      </Grid>

      <HrGray
        sx={{
          margin: "1.5rem 0",
        }}
      />
    </div>
  );
};

BasketProduct.PriceCounter = PriceCounter;
BasketProduct.Delete = Delete;
BasketProduct.Buy = Buy;
BasketProduct.BrandAndDesc = ProductBrandAndDesc;
BasketProduct.Price = Price;

export { BasketProduct };
