import React from "react";
import { Grid } from "@mui/material";
import { Comment } from "./components/Comment";
import { Promocode } from "./components/Promocode";
import { Discount } from "./components/Discount";
import { Delivery } from "./components/Delivery";
import { Total } from "./components/Total";

interface IBasketLayout {
  children?: React.ReactNode;
}

const BasketLayout = ({ children }: IBasketLayout) => {
  return (
    <Grid
      direction="column"
      container
      spacing={1}
      wrap="nowrap"
      sx={{
        padding: "0 5px",
        marginBottom: "20px",
      }}
    >
      {children}
    </Grid>
  );
};

BasketLayout.Comment = Comment;
BasketLayout.Promocode = Promocode;
BasketLayout.Discount = Discount;
BasketLayout.Delivery = Delivery;
BasketLayout.Total = Total;

export { BasketLayout };
