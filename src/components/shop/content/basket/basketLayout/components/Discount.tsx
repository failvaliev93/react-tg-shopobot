import React from "react";
import { TextField, Grid, Typography } from "@mui/material";

interface IDiscount {
  discount: number;
}

export const Discount = ({ discount }: IDiscount) => {
  return (
    <Grid
      item
      direction="row"
      container
      spacing={1}
      justifyContent="space-between"
      wrap="nowrap"
    >
      <Grid item>
        <Typography color="green">Скидка</Typography>
      </Grid>
      <Grid item>
        <Typography color="green">{discount}%</Typography>
      </Grid>
    </Grid>
  );
};
