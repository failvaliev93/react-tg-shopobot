import React from "react";
import { Grid, Typography } from "@mui/material";
import { DeliveryIcon } from "../../../../../UI/icons/DeliveryIcon";

interface IDelivery {
  delivery: number;
}

export const Delivery = ({ delivery }: IDelivery) => {
  return (
    <Grid
      item
      direction="row"
      container
      spacing={1}
      justifyContent="space-between"
      wrap="nowrap"
    >
      <Grid item>
        <Typography>
          Доставка <DeliveryIcon />
        </Typography>
      </Grid>
      <Grid item>
        <Typography>{delivery}₽</Typography>
      </Grid>
    </Grid>
  );
};
