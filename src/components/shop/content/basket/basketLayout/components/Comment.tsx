import React from "react";
import { TextField, Grid } from "@mui/material";

interface IComment {
  text: string;
  onChange: (text: string) => void;
}

export const Comment = ({ text, onChange }: IComment) => {
  return (
    <Grid item>
      <TextField
        multiline
        minRows={2}
        placeholder="Комментарий..."
        variant="outlined"
        fullWidth
        value={text}
        onChange={(e) => onChange(e.target.value)}
      />
    </Grid>
  );
};
