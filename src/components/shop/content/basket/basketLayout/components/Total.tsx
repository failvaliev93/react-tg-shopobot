import React from "react";
import { Grid, Typography } from "@mui/material";

interface ITotal {
  total: number;
}

export const Total = ({ total }: ITotal) => {
  return (
    <Grid
      item
      direction="row"
      container
      spacing={1}
      justifyContent="space-between"
      wrap="nowrap"
    >
      <Grid item>
        <Typography variant="h6">Итого</Typography>
      </Grid>
      <Grid item>
        <Typography variant="h6">{total}₽</Typography>
      </Grid>
    </Grid>
  );
};
