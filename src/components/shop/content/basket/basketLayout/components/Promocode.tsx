import React from "react";
import { TextField, Grid, Typography } from "@mui/material";

interface IPromocode {
  promocode: string;
  onChange: (promocode: string) => void;
}

export const Promocode = ({ promocode, onChange }: IPromocode) => {
  return (
    <Grid
      item
      direction="row"
      container
      spacing={1}
      justifyContent="space-between"
      columns={2}
      wrap="nowrap"
      alignItems="center"
    >
      <Grid item>
        <Typography>Промокод</Typography>
      </Grid>
      <Grid item>
        <TextField
          placeholder="PROMOCODE"
          variant="outlined"
          size="small"
          value={promocode}
          onChange={(e) => onChange(e.target.value)}
        />
      </Grid>
    </Grid>
  );
};
