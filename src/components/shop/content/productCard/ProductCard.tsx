import React from "react";
import s from "./ProductCard.module.css";
import { Сarousel } from "../../../UI/product/carousel/Сarousel";
import Discount from "../../../UI/product/discount/Discount";
import Price from "../../../UI/product/price/Price";
import { Product } from "../../../../types/Product";
import { SemiHiddenText } from "../../../UI/product/semiHiddenText/SemiHiddenText";
import { SizeButtonsOneSelect } from "../../../UI/product/sizeButtons/SizeButtonsOneSelect";
import { ColorButtonsOneSelect } from "../../../UI/product/colorButtons/ColorButtonsOneSelect";
import { FavoriteWithHandler } from "../../../UI/product/favorite/FavoriteWithHandler";

interface IProductCard {
  product: Product;
}

export const ProductCard = ({ product }: IProductCard) => {
  const [selectedSize, setSelectedSize] = React.useState<string | null>(null);
  const [selectedColor, setSelectedColor] = React.useState<string | null>(null);

  const sizeHandler = (size: string) => setSelectedSize(size);
  const colorHandler = (color: string) => setSelectedColor(color);

  return (
    <div className={s.card}>
      <div className={s.previewBlock}>
        {product?.images && <Сarousel imagesURL={product.images} />}
        {product?.discount && (
          <div className={s.discountWrap}>
            <Discount perCount={product.discount} />
          </div>
        )}
        <div className={s.favWrap}>
          <FavoriteWithHandler product={product} />
        </div>
      </div>
      <div className={s.info}>
        <div className={s.brandAndPrice}>
          <div className={s.brand}>{product.brand}</div>
          <Price
            value={product.price}
            discount={product.discount}
            sx={{
              fontSize: "22px",
              lineHeight: "27px",
            }}
          />
        </div>
        <p className={s.description}>{product.description}</p>
        <hr className={s.hr} />
        <SemiHiddenText text={product.text} />
      </div>
      <div className={s.properties}>
        {product.colors && (
          <>
            <p className={s.propName}>Цвет</p>
            <ColorButtonsOneSelect
              onSelect={colorHandler}
              colors={product.colors}
              selectedColor={product.colors[0]}
            />
          </>
        )}
        <p className={s.propName}>Размер</p>
        <SizeButtonsOneSelect
          onSelect={sizeHandler}
          sizes={product.sizes}
          selectedSize={product.sizes[0]}
        />
      </div>
    </div>
  );
};
