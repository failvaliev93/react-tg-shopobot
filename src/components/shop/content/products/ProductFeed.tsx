import { Typography } from "@mui/material";
import React from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { fetchProductsParams } from "../../../../API/ProductAPI";
import { fetchProducts } from "../../../../features/shop/shopSlice";
import { useAppDispatch } from "../../../../redux/hooks";
import { Product } from "../../../../types/Product";
import { FilterParam, ToolBar } from "./toolBar/ToolBar";
import { ProductList } from "./ProductList/ProductList";
import { ProductListFormat } from "./ProductList/ProductList";

const initQueryParams: fetchProductsParams = {
  category: "Кроссовки",
  properties: {
    sort: "priceLow",
    discount: "all",
    gender: "all",
  },
  limit: 10,
  page: 1,
};

interface ProductFeedProps {
  queryParams?: fetchProductsParams;
}

export const ProductFeed = (props: ProductFeedProps) => {
  const dispatch = useAppDispatch();

  const [products, setProducts] = React.useState<Array<Product>>([]);
  const [format, setFormat] = React.useState<ProductListFormat>("2x");
  const [filterParam, setfilterParam] = React.useState<FilterParam | null>(
    null
  );
  const [errorText, setErrorText] = React.useState<string | null>(null);
  const productsCount = React.useRef(0);
  const pages = React.useRef<number>(0);
  const maxPages = React.useRef<number>(0);

  //init
  React.useEffect(() => {
    if (filterParam === null) {
      const queryParams: fetchProductsParams = {
        ...initQueryParams,
        ...props.queryParams,
      };

      dispatch(fetchProducts(queryParams))
        .then((res: any) => {
          // console.log(res, queryParams);
          if (res.meta.requestStatus !== "rejected") {
            const params: FilterParam = {
              sizesAll: [],
              sizesSelected: [],
              sizesAviable: [],
              colorsAviable: [],
              colorsSelected: [],
              discount: queryParams.properties?.discount || "all",
              priceLimitMin: 0,
              priceLimitMax: 1_000_000,
              priceMin: 0,
              priceMax: 100000,
              gender: queryParams.properties?.gender || "all",
              sort: queryParams.properties?.sort || "priceHigh",
            };

            const fp = createFilterParam(res.payload.list);
            const newFParams: FilterParam = {
              ...params,
              ...fp,
              priceLimitMin: res.payload.priceLimitMin,
              priceLimitMax: res.payload.priceLimitMax,
              priceMin: res.payload.priceLimitMin,
              priceMax: res.payload.priceLimitMax,
              sizesAll: res.payload.sizesAll,
              colorsAviable: res.payload.colorsAll,
            };

            setfilterParam(newFParams);
            setProducts(res.payload.list);
            productsCount.current = res.payload.productsCount;
            pages.current = Number(queryParams.page);
            maxPages.current = res.payload.maxPages;
          } else {
            setErrorText("Ошибка получения данных");
          }
        })
        .catch((reason) => {
          console.warn(reason);
          setErrorText("Ошибка получения данных");
        });
    }
  }, [filterParam]);

  React.useEffect(() => {
    if (filterParam) {
      filterParamHandler(filterParam);
    }
  }, [props.queryParams]);

  const formatHandler = (format: ProductListFormat) => setFormat(format);

  const filterParamHandler = (params: FilterParam) => {
    fetchProductsWrapper(params).then((res) => {
      if (res) {
        setProducts(res);
      }
    });
  };

  const fetchProductsWrapper = async (
    params: FilterParam
  ): Promise<Array<Product> | null> => {
    const queryParams: fetchProductsParams = {
      ...initQueryParams,
      ...props.queryParams,
      page: pages.current,
      properties: {
        sizes: params.sizesSelected,
        colors: params.colorsSelected,
        discount: params.discount,
        gender: params.gender,
        price: {
          min: params.priceMin,
          max: params.priceMax,
        },
        sort: params.sort,
      },
    };
    // console.log(queryParams, params);

    return new Promise((resolve, rejected) => {
      dispatch(fetchProducts(queryParams))
        .then((res: any) => {
          if (res.meta.requestStatus !== "rejected") {
            const fp = createFilterParam(res.payload.list);
            const newFParams = {
              ...params,
              ...fp,
              priceLimitMin: res.payload.priceLimitMin,
              priceLimitMax: res.payload.priceLimitMax,
              sizesAll: res.payload.sizesAll,
              colorsAviable: res.payload.colorsAll,
            };

            setfilterParam(newFParams);
            productsCount.current = res.payload.productsCount;
            pages.current = res.payload.page;
            maxPages.current = res.payload.maxPages;

            resolve(res.payload.list);
          } else {
            setErrorText("Ошибка получения данных");
            return rejected(null);
          }
        })
        .catch((reason) => {
          console.warn(reason);
          setErrorText("Ошибка получения данных");
          return rejected(null);
        });
    });
  };

  const createFilterParam = (products: Array<Product>): FilterParam => {
    // let colors: Array<Color> = [];
    // products.forEach((itm: Product) =>
    //   itm.colors?.forEach((c) => !colors.includes(c) && colors.push(c))
    // );

    let sizes: Array<string> = [];
    products.forEach((itm: Product) =>
      itm.sizes?.forEach((s) => !sizes.includes(s) && sizes.push(s))
    );
    // console.log(sizes);

    return {
      // colorsAviable: colors,
      sizesAviable: sizes,
    } as FilterParam;
  };

  const fetchData = () => {
    if (pages.current < maxPages.current) {
      pages.current++;
      if (filterParam) {
        fetchProductsWrapper(filterParam).then((res) => {
          // console.log(res);
          if (res) {
            setProducts([...products, ...res]);
          }
        });
      }
    }
  };

  return (
    <>
      {filterParam && (
        <div>
          <ToolBar
            productsCount={productsCount.current}
            format={format}
            filterParam={filterParam}
            setFormat={formatHandler}
            setFilterParam={filterParamHandler}
            onResetFilter={() => setfilterParam(null)}
          />
          <InfiniteScroll
            dataLength={products.length}
            hasMore={pages.current < maxPages.current}
            next={fetchData}
            loader={<p>Loading...</p>}
          >
            <ProductList products={products} format={format} />
          </InfiniteScroll>
        </div>
      )}
      {errorText && (
        <Typography
          sx={{
            "&": {
              marginTop: "1rem",
            },
          }}
        >
          {errorText}
        </Typography>
      )}
    </>
  );
};
