import React from "react";
import { Product } from "../../../../../../types/Product";
import { SmallView } from "../listViews/SmallView";
import s from "./NotFound.module.css";

interface NotFoundProps {
  recomendatedProducts?: Array<Product>;
}

export const NotFound = (props: NotFoundProps) => {
  return (
    <div>
      <div className={s.notFound}>
        <img src="/img/icons/feather_search.svg" alt="feather search" />
        <p>Товары не найдены</p>
      </div>
      {props.recomendatedProducts && (
        <>
          <div className={s.interest}>
            <p>Возможно вас заинтересует</p>
          </div>
          <SmallView products={props.recomendatedProducts} />
        </>
      )}
    </div>
  );
};
