import React from "react";
import { Link } from "react-router-dom";
import { Product } from "../../../../../../types/Product";
import { FavoriteWithHandler } from "../../../../../UI/product/favorite/FavoriteWithHandler";
import Price from "../../../../../UI/product/price/Price";
import s from "../ProductList.module.css";

interface ListItemProps {
  product: Product;
  isText?: boolean;
}

export const ListItem = ({ product, isText = false }: ListItemProps) => {
  return (
    <div className={s.product}>
      <div className={s.preview}>
        <img src={product.images[0]} alt={product.brand} />
        <div className={s.favoriteWrap}>
          <FavoriteWithHandler product={product} />
        </div>
      </div>
      <div className={s.info}>
        <Link
          to={`/catalog/${product.type.adres}/${product.id}`}
          className={s.link}
        >
          <div className={s.brand}>{product.brand}</div>
          <div className={s.description}>{product.description}</div>
          {isText && <p className={s.text}>{product.text}</p>}
          <div className={s.price}>
            <Price value={product.price} discount={product.discount} />
          </div>
        </Link>
      </div>
    </div>
  );
};
