import React from "react";
import { Product } from "../../../../../../types/Product";
import Discount from "../../../../../UI/product/discount/Discount";
import s from "../ProductList.module.css";
import { ListItem } from "./ListItem";

interface BigViewProps {
  products: Array<Product>;
}

export const BigView = ({ products }: BigViewProps) => {
  return (
    <div className={s.ProductListItemBig}>
      {products.map((product, i) => {
        return (
          <div key={i} className={s.itemWrap}>
            <ListItem product={product} isText />
            {product.discount && (
              <div className={s.discountWrap}>
                <Discount perCount={product.discount} />
              </div>
            )}
          </div>
        );
      })}
    </div>
  );
};
