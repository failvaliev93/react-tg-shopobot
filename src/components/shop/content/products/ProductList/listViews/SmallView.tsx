import React from "react";
import { Product } from "../../../../../../types/Product";
import Discount from "../../../../../UI/product/discount/Discount";
import s from "../ProductList.module.css";
import { ListItem } from "./ListItem";

interface SmallViewProps {
  products: Array<Product>;
}

export const SmallView = ({ products }: SmallViewProps) => {
  return (
    <div className={s.ProductListItemSmall}>
      {products.map((product, i) => {
        return (
          <div key={i} className={s.itemWrap}>
            <ListItem product={product} />
            {product.discount && (
              <div className={s.discountWrap}>
                <Discount perCount={product.discount} />
              </div>
            )}
          </div>
        );
      })}
    </div>
  );
};
