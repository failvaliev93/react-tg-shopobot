import React from "react";
import { Product } from "../../../../../types/Product";
import { BigView } from "./listViews/BigView";
import { SmallView } from "./listViews/SmallView";
import { NotFound } from "./notFound/NotFound";
import s from "./ProductList.module.css";

export type ProductListFormat = "big" | "2x";

interface ProductListProps {
  products: Array<Product>;
  format?: ProductListFormat;
  recomendatedProducts?: Array<Product>;
}

export const ProductList = ({
  products,
  format = "2x",
  recomendatedProducts,
}: ProductListProps) => {
  if (products.length === 0) {
    return <NotFound recomendatedProducts={recomendatedProducts} />;
  }

  if (format === "big") {
    return <BigView products={products} />;
  } else {
    return <SmallView products={products} />;
  }
};
