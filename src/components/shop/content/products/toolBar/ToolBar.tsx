import React from "react";
import { DiscountSelector, Gender } from "../../../../../types/Product";
import { SortBy } from "../../../../../types/toolBar";
import { ProductListFormat } from "../ProductList/ProductList";
import s from "./ToolBar.module.css";
import { FilterModal } from "./filterModal/FilterModal";
import { SortModal } from "./sortModal/SortModal";
import { Button } from "@mui/material";

export type FilterParam = {
  sizesAll: Array<string>;
  sizesSelected: Array<string>;
  sizesAviable: Array<string>;
  colorsAviable: Array<string>;
  colorsSelected: Array<string>;
  discount: DiscountSelector;
  priceLimitMin: number;
  priceLimitMax: number;
  priceMin: number;
  priceMax: number;
  gender: Gender | "all";
  sort: SortBy;
};

interface ToolBarProps {
  format: ProductListFormat;
  filterParam: FilterParam;
  productsCount: number;
  setFilterParam: (filterParam: FilterParam) => void;
  setFormat: (format: ProductListFormat) => void;
  onResetFilter: () => void;
}

export const ToolBar = ({ filterParam, ...props }: ToolBarProps) => {
  const [format, setFormat] = React.useState(props.format);
  const [filterWindow, setFilterWindow] = React.useState(false);
  const [sortWindow, setSortWindow] = React.useState(false);

  // console.log("Filter:", filterParam);

  const formatHandler = () => {
    if (format === "2x") {
      setFormat("big");
      props.setFormat("big");
    } else if (format === "big") {
      setFormat("2x");
      props.setFormat("2x");
    }
  };

  const sortHandler = (sort: SortBy) =>
    filterModalHandler({ ...filterParam, sort: sort });

  const onFilterWindowClose = () => setFilterWindow(!filterWindow);
  const onSortWindowClose = () => setSortWindow(!sortWindow);

  const filterModalHandler = (params: FilterParam) =>
    props.setFilterParam(params);

  const sizesHandler = (selected: boolean, size: string) => {
    if (filterParam) {
      if (selected) {
        filterModalHandler({
          ...filterParam,
          sizesSelected: [...filterParam.sizesSelected, size],
        });
      } else {
        filterModalHandler({
          ...filterParam,
          sizesSelected: filterParam.sizesSelected.filter(
            (_size) => _size !== size
          ),
        });
      }
    }
  };

  const colorsHandler = (checked: boolean, color: string) => {
    if (filterParam) {
      if (checked) {
        filterModalHandler({
          ...filterParam,
          colorsSelected: [...filterParam.colorsSelected, color],
        });
      } else {
        filterModalHandler({
          ...filterParam,
          colorsSelected: filterParam.colorsSelected.filter(
            (_size) => _size !== color
          ),
        });
      }
    }
  };

  return (
    <>
      <div className={s.filter}>
        <p>{props.productsCount} товаров</p>
        <div className={s.bblock}>
          {format === "big" && (
            <img
              src="/img/icons/filter-format-4x.svg"
              alt="format-4x"
              onClick={formatHandler}
            />
          )}
          {format === "2x" && (
            <img
              src="/img/icons/filter-format-2x.svg"
              alt="format-2x"
              onClick={formatHandler}
            />
          )}
          <img
            src="/img/icons/filter-reverse.svg"
            alt="reverse"
            onClick={onSortWindowClose}
          />
          <img
            src="/img/icons/filter.svg"
            alt="filter"
            onClick={onFilterWindowClose}
          />
        </div>
      </div>
      {filterWindow && (
        <FilterModal onClose={onFilterWindowClose}>
          {filterParam !== null && (
            <>
              <Button variant="outlined" onClick={() => props.onResetFilter()}>
                Сбросить
              </Button>
              <FilterModal.Price
                limitMin={filterParam.priceLimitMin}
                limitMax={filterParam.priceLimitMax}
                min={filterParam.priceMin}
                max={filterParam.priceMax}
                setMin={(value) =>
                  filterModalHandler({ ...filterParam, priceMin: value })
                }
                setMax={(value) =>
                  filterModalHandler({ ...filterParam, priceMax: value })
                }
              />
              <FilterModal.Discount
                value={filterParam.discount}
                onChange={(value) =>
                  filterModalHandler({ ...filterParam, discount: value })
                }
              />
              <FilterModal.Colors
                values={filterParam.colorsAviable}
                checked={filterParam.colorsSelected}
                onChange={colorsHandler}
              />
              <FilterModal.Sizes
                values={filterParam.sizesAll}
                selected={filterParam.sizesSelected}
                anabled={filterParam.sizesAviable}
                onChange={sizesHandler}
              />
              <FilterModal.Gender
                value={filterParam.gender}
                onChange={(value) =>
                  filterModalHandler({ ...filterParam, gender: value })
                }
              />
            </>
          )}
        </FilterModal>
      )}
      {sortWindow && (
        <SortModal onChange={sortHandler} onClose={onSortWindowClose} />
      )}
    </>
  );
};
