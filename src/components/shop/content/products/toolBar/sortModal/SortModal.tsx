import { ListItemButton, ListItemText } from "@mui/material";
import React from "react";
import { SortBy } from "../../../../../../types/toolBar";
import BottomWindow from "../../../../../UI/modals/bottomModal/BottomModal";

interface SortModalProps {
  onClose: () => void;
  onChange: (sort: SortBy) => void;
}

export const SortModal = (props: SortModalProps) => {
  const onChangePriceHigh = () => {
    props.onChange("priceHigh");
    props.onClose();
  };
  const onChangePriceLow = () => {
    props.onChange("priceLow");
    props.onClose();
  };

  return (
    <BottomWindow name="Сортировать" onClose={props.onClose}>
      <ListItemButton component="span" onClick={onChangePriceHigh}>
        <ListItemText primary="Цена - сначала дороже" />
      </ListItemButton>
      <ListItemButton component="span" onClick={onChangePriceLow}>
        <ListItemText primary="Цена - сначала дешевле" />
      </ListItemButton>
    </BottomWindow>
  );
};
