import React from "react";
import BottomWindow from "../../../../../UI/modals/bottomModal/BottomModal";
import s from "./FilterModal.module.css";
import { Price } from "./components/Price";
import { Discount } from "./components/Discount";
import { Colors } from "./components/Colors";
import Sizes from "./components/Sizes";
import { Gender } from "./components/Gender";

interface FilterModalProps {
  onClose: () => void;
  children?: React.ReactNode;
}

const FilterModal = (props: FilterModalProps) => {
  return (
    <BottomWindow name="Фильтры" onClose={props.onClose}>
      {props.children}
    </BottomWindow>
  );
};

FilterModal.Price = Price;
FilterModal.Discount = Discount;
FilterModal.Colors = Colors;
FilterModal.Sizes = Sizes;
FilterModal.Gender = Gender;

export { FilterModal };
