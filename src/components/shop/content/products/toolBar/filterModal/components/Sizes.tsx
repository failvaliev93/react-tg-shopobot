import React from "react";
import SizeToggleButton from "../../../../../../UI/buttons/sizeToggleButton/SizeToggleButton";
import s from "../FilterModal.module.css";

interface SizesProps {
  values: Array<string>;
  selected: Array<string>;
  anabled: Array<string>;
  onChange: (selected: boolean, size: string) => void;
}

const Sizes = (props: SizesProps) => {
  return (
    <details>
      <summary>Размер</summary>
      <div className={s.sizes}>
        {props.values.map((size, i) => {
          return (
            <SizeToggleButton
              key={i}
              size={size}
              onChange={(selected) => props.onChange(selected, size)}
              selected={props.selected.includes(size)}
              disabled={!props.anabled.includes(size)}
            />
          );
        })}
      </div>
    </details>
  );
};

export default Sizes;
