import {
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup,
} from "@mui/material";
import React from "react";
import { DiscountSelector } from "../../../../../../../types/Product";

interface DiscountProps {
  value: DiscountSelector;
  onChange: (value: DiscountSelector) => void;
}

export const Discount = (props: DiscountProps) => {
  const onChange = (
    event: React.ChangeEvent<HTMLInputElement>,
    value: string
  ) => {
    props.onChange(value as DiscountSelector);
  };

  return (
    <details>
      <summary>Скидка</summary>
      <FormControl>
        <RadioGroup
          aria-labelledby="demo-controlled-radio-buttons-group"
          name="controlled-radio-buttons-group"
          value={props.value}
          onChange={onChange}
        >
          <FormControlLabel
            value="all"
            control={<Radio size="small" />}
            label="Все"
          />
          <FormControlLabel
            value="withDiscount"
            control={<Radio size="small" />}
            label="Со скидкой"
          />
          <FormControlLabel
            value="withoutDiscount"
            control={<Radio size="small" />}
            label="Без скидки"
          />
        </RadioGroup>
      </FormControl>
    </details>
  );
};
