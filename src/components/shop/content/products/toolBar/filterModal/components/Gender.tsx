import {
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup,
} from "@mui/material";
import React from "react";
import { Gender as GenderT } from "../../../../../../../types/Product";

interface GenderProps {
  value: GenderT | "all";
  onChange: (value: GenderT) => void;
}

export const Gender = (props: GenderProps) => {
  const onChange = (
    event: React.ChangeEvent<HTMLInputElement>,
    value: string
  ) => {
    props.onChange(value as GenderT);
  };

  return (
    <details>
      <summary>Пол</summary>
      <FormControl>
        <RadioGroup
          aria-labelledby="demo-controlled-radio-buttons-group"
          name="controlled-radio-buttons-group"
          value={props.value}
          onChange={onChange}
        >
          <FormControlLabel
            value="all"
            control={<Radio size="small" />}
            label="Все"
          />
          <FormControlLabel
            value="m"
            control={<Radio size="small" />}
            label="Мужской"
          />
          <FormControlLabel
            value="f"
            control={<Radio size="small" />}
            label="Женский"
          />
        </RadioGroup>
      </FormControl>
    </details>
  );
};
