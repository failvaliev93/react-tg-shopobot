import React from "react";
import ColorCheckbox from "../../../../../../UI/checkboxes/color/ColorCheckbox";
import s from "../FilterModal.module.css";

interface ColorsProps {
  values: Array<string>;
  checked: Array<string>;
  onChange: (checked: boolean, color: string) => void;
}

export const Colors = (props: ColorsProps) => {
  // console.log(props.values);

  return (
    <details>
      <summary>Цвет</summary>
      <div className={s.colors}>
        {props.values.map((color, i) => (
          <ColorCheckbox
            key={i}
            color={color}
            checked={props.checked.includes(color)}
            onChange={(checked) => props.onChange(checked, color)}
          />
        ))}
      </div>
    </details>
  );
};
