import React from "react";
import { Slider } from "@mui/material";
import PriceMaskInput from "../../../../../../UI/inputs/priceMaskInput/PriceMaskInput";
import s from "../FilterModal.module.css";
import { useDebouncedCallback } from "use-debounce";
import { inputDelay } from "../../../../../../../consts/setup";

const delay = inputDelay;

interface PriceProps {
  min: number;
  max: number;
  limitMin: number;
  limitMax: number;
  setMin: (value: number) => void;
  setMax: (value: number) => void;
}

export const Price = (props: PriceProps) => {
  const [min, setMin] = React.useState<number>(props.min);
  const [max, setMax] = React.useState<number>(props.max);
  const debouncedSetMin = useDebouncedCallback(
    (min) => props.setMin(min),
    delay
  );
  const debouncedSetMax = useDebouncedCallback(
    (max) => props.setMax(max),
    delay
  );

  // React.useEffect(() => setMax(props.max), [props.max]);
  // React.useEffect(() => setMax(props.min), [props.min]);
  // React.useEffect(() => debouncedSetMax(props.max), [max]);
  // React.useEffect(() => debouncedSetMin(props.min), [min]);

  const priceSliderOnChange = (event: Event, newValue: number | number[]) => {
    const newMin = typeof newValue === "number" ? newValue : newValue[0];
    const newMax = typeof newValue === "number" ? newValue : newValue[1];
    setMin(newMin);
    setMax(newMax);
    if (newMin !== min) debouncedSetMin(newMin);
    if (newMax !== max) debouncedSetMax(newMax);
  };

  const minPriceOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value: number = +e.target.value;
    if (value >= props.limitMin && value <= max) {
      setMin(value);
      if (value !== min) debouncedSetMin(value);
    }
  };

  const maxPriceOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value: number = +e.target.value;
    if (value >= min && value <= props.limitMax) {
      setMax(value);
      if (value !== max) debouncedSetMax(value);
    }
  };

  return (
    <div className={s.price}>
      <p>Цена</p>
      <div className={s.inputs}>
        <PriceMaskInput value={min} onChange={minPriceOnChange} />
        <img src="/img/icons/line-mini.svg" alt="line" />
        <PriceMaskInput value={max} onChange={maxPriceOnChange} />
      </div>
      <div className={s.slider}>
        <Slider
          value={[min, max]}
          onChange={priceSliderOnChange}
          valueLabelDisplay="auto"
          min={props.limitMin}
          max={props.limitMax}
          size="small"
          disableSwap
        />
      </div>
    </div>
  );
};
