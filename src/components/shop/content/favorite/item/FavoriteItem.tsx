import React from "react";
import { Product } from "../../../../../types/Product";
import { BasketProduct as BasketProductComp } from "../../basket/basketItem/basketProduct/BasketProduct";

interface IFavoriteItem {
  product: Product;
  onDelete: (product: Product) => void;
  onBuy: (product: Product) => void;
  inShop?: boolean;
}

export const FavoriteItem = ({
  product,
  onBuy,
  onDelete,
  inShop,
}: IFavoriteItem) => {
  const deleteHandler = () => onDelete(product);
  const buyHandler = () => onBuy(product);

  return (
    <div>
      <BasketProductComp
        imgSrc={product.images[0]}
        deleteComponent={<BasketProductComp.Delete onClick={deleteHandler} />}
        buyComponent={
          <BasketProductComp.Buy onClick={buyHandler} bought={inShop} />
        }
        brandAndDescComponent={
          <BasketProductComp.BrandAndDesc
            brand={product.brand}
            description={product.description}
          />
        }
        priceComponent={
          <BasketProductComp.Price
            value={product.price}
            sx={{
              fontSize: "20px",
              fontWeight: 500,
              lineHeight: "24px",
            }}
          />
        }
      />
    </div>
  );
};
