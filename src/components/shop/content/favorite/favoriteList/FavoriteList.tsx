import React from "react";
import { BasketProduct } from "../../../../../API/BasketAPI";
import { Product } from "../../../../../types/Product";
import { FavoriteItem } from "../item/FavoriteItem";

interface IFavoriteList {
  items: Array<Product>;
  inShopItems: Array<BasketProduct>;
  onDeleteItem: (product: Product) => void;
  onBuyItem: (product: Product) => void;
}

export const FavoriteList = ({
  items,
  inShopItems,
  onBuyItem,
  onDeleteItem,
}: IFavoriteList) => {
  return (
    <>
      {items.map((prod) => (
        <FavoriteItem
          onDelete={onDeleteItem}
          onBuy={onBuyItem}
          product={prod}
          key={prod.id}
          inShop={Boolean(
            inShopItems.find(
              (shoppedProd) => shoppedProd.product.id === prod.id
            )
          )}
        />
      ))}
    </>
  );
};
