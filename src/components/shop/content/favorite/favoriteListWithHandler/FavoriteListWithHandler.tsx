import React from "react";
import {
  addBasketAC,
  delBasketAC,
  delFavoriteAC,
} from "../../../../../features/shop/shopSlice";
import { useBasket } from "../../../../../hooks/useBasket";
import { useFavorites } from "../../../../../hooks/useFavorites";
import { useAppDispatch } from "../../../../../redux/hooks";
import { Product } from "../../../../../types/Product";
import { NotifyBottom } from "../../../../UI/notify/NotifyBottom";
import { NotFound } from "../../products/ProductList/notFound/NotFound";
import { FavoriteList } from "../favoriteList/FavoriteList";

export const FavoriteListWithHandler = () => {
  const dispatch = useAppDispatch();
  const favoritesProd = useFavorites();
  const basket = useBasket();

  const [open, setOpen] = React.useState(false);

  const onDeleteHandler = (prod: Product) => {
    dispatch(delFavoriteAC(prod));
  };
  const onBuyHandler = (prod: Product) => {
    const has = basket?.find((bProd) => bProd.product.id === prod.id);

    if (has) {
      dispatch(
        delBasketAC({
          product: prod,
          count: 1,
        })
      );
    } else {
      dispatch(
        addBasketAC({
          product: prod,
          count: 1,
        })
      );
      setOpen(true);
    }
  };

  const handleClose = () => setOpen(false);

  if (!favoritesProd || !favoritesProd.length) {
    return <NotFound />;
  }

  return (
    <>
      <FavoriteList
        items={favoritesProd}
        inShopItems={basket || []}
        onBuyItem={onBuyHandler}
        onDeleteItem={onDeleteHandler}
      />
      <NotifyBottom
        open={open}
        text="Ваш заказ добавлен в корзину!"
        handleClose={handleClose}
      />
    </>
  );
};
