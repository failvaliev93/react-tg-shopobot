import {
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Typography,
} from "@mui/material";
import React from "react";
import { Link } from "react-router-dom";
import { ICategory } from "../../../../API/CategoriesAPI";
import { ArrowNavRightIcon } from "../../../UI/icons/ArrowNavRightIcon";

interface CategoriesProps {
  items: Array<ICategory>;
  onSelect: (cat: ICategory) => void;
}

export const Categories: React.FC<CategoriesProps> = (
  props: CategoriesProps
) => {
  return (
    <List>
      {props.items.map((cat, i) => (
        <ListItem disablePadding key={i}>
          <ListItemButton
            onClick={() => props.onSelect(cat)}
            sx={{
              "&": {
                paddingTop: "14px",
                paddingBottom: "14px",
              },
            }}
            component={Link}
            to={`/catalog/${cat.adres}`}
          >
            <ListItemText>
              <Typography
                sx={{
                  "&": {
                    fontSize: "0.9rem",
                    fontWeight: 500,
                  },
                }}
              >
                {cat.name}
              </Typography>
            </ListItemText>
            <ListItemIcon
              sx={{
                "&": {
                  minWidth: "10px",
                },
              }}
            >
              <ArrowNavRightIcon />
            </ListItemIcon>
          </ListItemButton>
        </ListItem>
      ))}
    </List>
  );
};
