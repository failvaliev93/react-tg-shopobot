import classNames from "classnames";
import React from "react";
import s from "./BottomBar.module.css";
import { Link, useMatch } from "react-router-dom";
import { useBasket } from "../../../hooks/useBasket";

interface LinkChipProps {
  value: number;
}

const LinkChip = ({ value }: LinkChipProps) => {
  return <div className={s.linkChip}>{value}</div>;
};

interface BottomBarLinkProps {
  img: string;
  url: string;
  alt?: string;
  chip?: number;
}
const BottomBarLink = ({ img, url, alt, chip }: BottomBarLinkProps) => {
  const match = useMatch({
    path: url,
    end: url.length === 1,
  });
  const linkChip = chip && chip > 0 ? <LinkChip value={chip} /> : null;

  return (
    <Link
      to={url}
      className={
        match
          ? classNames(s.bottomBarLink, s.bottomBarLinkActive)
          : s.bottomBarLink
      }
    >
      {linkChip}
      <img src={img} alt={alt} />
    </Link>
  );
};

export const BottomBar = () => {
  const basket = useBasket();

  return (
    <div className={s.bottomBar}>
      <BottomBarLink img="/img/icons/home.svg" url="/" alt="home" />
      <BottomBarLink img="/img/icons/search.svg" url="/catalog" alt="search" />
      <BottomBarLink
        img="/img/icons/favs.svg"
        url="/favorite"
        alt="favorites"
      />
      <BottomBarLink
        img="/img/icons/basket.svg"
        url="/basket"
        alt="basket"
        chip={basket ? basket?.length : 0}
      />
    </div>
  );
};
