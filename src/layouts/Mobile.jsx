import React from "react";
import { BottomBar } from "../components/shop/bottomBar/BottomBar";
import { Content } from "../components/shop/content/Content";

export const Mobile = () => {
  return (
    <>
      <Content />
      <BottomBar />
    </>
  );
};
