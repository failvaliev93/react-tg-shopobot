import { ICategory } from "../API/CategoriesAPI";

export type Gender = "m" | "f" | "mf";

export type DiscountSelector = "withDiscount" | "withoutDiscount" | "all";

export type Product = {
  id: string;
  type: ICategory;
  brand: string;
  description: string;
  text: string;
  price: number;
  sizes: Array<string>;
  gender: Gender;
  discount?: number; //скидка
  images: Array<string>;
  color?: string;
  colors?: Array<string>;
};
